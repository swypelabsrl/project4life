-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Creato il: Mag 08, 2019 alle 10:32
-- Versione del server: 10.0.38-MariaDB-cll-lve
-- Versione PHP: 7.2.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `idthrnff_project4life`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_clients`
--

DROP TABLE IF EXISTS `#__project_clients`;
CREATE TABLE `#__project_clients` (
  `idClient` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `cognome` varchar(50) NOT NULL,
  `dataNascita` date NOT NULL,
  `professione` text NOT NULL,
  `ral` decimal(10,0) NOT NULL,
  `prevObbli` int(11) NOT NULL,
  `prevLavoro` int(11) NOT NULL,
  `prevPriv` int(11) NOT NULL,
  `luogo` varchar(50) NOT NULL,
  `data` date NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_erog_prev_lavoro`
--

DROP TABLE IF EXISTS `#__project_erog_prev_lavoro`;
CREATE TABLE `#__project_erog_prev_lavoro` (
  `erogLavId` int(11) NOT NULL,
  `erogType` int(11) NOT NULL,
  `data` date NOT NULL,
  `valore` decimal(10,0) NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `prevLavId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_erog_prev_obblig`
--

DROP TABLE IF EXISTS `#__project_erog_prev_obblig`;
CREATE TABLE `#__project_erog_prev_obblig` (
  `erogObblId` int(11) NOT NULL,
  `erogType` int(11) NOT NULL,
  `data` date NOT NULL,
  `valore` decimal(10,0) NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `prevObblId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_erog_prev_privata`
--

DROP TABLE IF EXISTS `#__project_erog_prev_privata`;
CREATE TABLE `#__project_erog_prev_privata` (
  `erogPrivId` int(11) NOT NULL,
  `erogType` int(11) NOT NULL,
  `data` date NOT NULL,
  `valore` decimal(10,0) NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL,
  `prevPrivId` int(11) NOT NULL,
  `clientId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_prevlavoro`
--

DROP TABLE IF EXISTS `#__project_prevlavoro`;
CREATE TABLE `#__project_prevlavoro` (
  `prevLavId` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `primaContr` date NOT NULL,
  `ultimaContr` date NOT NULL,
  `anzianitaContr` int(11) NOT NULL,
  `anzianitaContrType` int(11) NOT NULL,
  `monteContr` int(11) NOT NULL,
  `renditaAcquisitaMens` decimal(10,0) NOT NULL,
  `renditaAcquisitaAnn` decimal(10,0) NOT NULL,
  `renditaAnnuaAggMens` decimal(10,0) NOT NULL,
  `renditaAnnuaAnn` decimal(10,0) NOT NULL,
  `clientId` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_prevobbligatorie`
--

DROP TABLE IF EXISTS `#__project_prevobbligatorie`;
CREATE TABLE `#__project_prevobbligatorie` (
  `prevObblId` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `primaContr` date NOT NULL,
  `ultimaContr` date NOT NULL,
  `anzianitaContr` int(11) NOT NULL,
  `anzianitaContrType` int(11) NOT NULL,
  `buchiContr` int(11) NOT NULL,
  `riscattabilita` int(11) NOT NULL,
  `monteContr` int(11) NOT NULL,
  `renditaAcquisitaMens` decimal(10,0) NOT NULL,
  `renditaAcquisitaAnn` decimal(10,0) NOT NULL,
  `renditaAnnuaAggMens` decimal(10,0) NOT NULL,
  `renditaAnnuaAnn` decimal(10,0) NOT NULL,
  `clientId` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `#__project_prevprivata`
--

DROP TABLE IF EXISTS `#__project_prevprivata`;
CREATE TABLE `#__project_prevprivata` (
  `prevLavId` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `primaContr` date NOT NULL,
  `ultimaContr` date NOT NULL,
  `anzianitaContr` int(11) NOT NULL,
  `anzianitaContrType` int(11) NOT NULL,
  `monteContr` int(11) NOT NULL,
  `renditaAcquisitaMens` decimal(10,0) NOT NULL,
  `renditaAcquisitaAnn` decimal(10,0) NOT NULL,
  `renditaAnnuaAggMens` decimal(10,0) NOT NULL,
  `renditaAnnuaAnn` decimal(10,0) NOT NULL,
  `clientId` int(11) NOT NULL,
  `datetime` datetime NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `#__project_clients`
--
ALTER TABLE `#__project_clients`
  ADD PRIMARY KEY (`idClient`);

--
-- Indici per le tabelle `#__project_erog_prev_lavoro`
--
ALTER TABLE `#__project_erog_prev_lavoro`
  ADD PRIMARY KEY (`erogLavId`);

--
-- Indici per le tabelle `#__project_erog_prev_obblig`
--
ALTER TABLE `#__project_erog_prev_obblig`
  ADD PRIMARY KEY (`erogObblId`);

--
-- Indici per le tabelle `#__project_erog_prev_privata`
--
ALTER TABLE `#__project_erog_prev_privata`
  ADD PRIMARY KEY (`erogPrivId`);

--
-- Indici per le tabelle `#__project_prevlavoro`
--
ALTER TABLE `#__project_prevlavoro`
  ADD PRIMARY KEY (`prevLavId`);

--
-- Indici per le tabelle `#__project_prevobbligatorie`
--
ALTER TABLE `#__project_prevobbligatorie`
  ADD PRIMARY KEY (`prevObblId`);

--
-- Indici per le tabelle `#__project_prevprivata`
--
ALTER TABLE `#__project_prevprivata`
  ADD PRIMARY KEY (`prevLavId`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `#__project_clients`
--
ALTER TABLE `#__project_clients`
  MODIFY `idClient` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `#__project_erog_prev_lavoro`
--
ALTER TABLE `#__project_erog_prev_lavoro`
  MODIFY `erogLavId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `#__project_erog_prev_obblig`
--
ALTER TABLE `#__project_erog_prev_obblig`
  MODIFY `erogObblId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `#__project_erog_prev_privata`
--
ALTER TABLE `#__project_erog_prev_privata`
  MODIFY `erogPrivId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `#__project_prevlavoro`
--
ALTER TABLE `#__project_prevlavoro`
  MODIFY `prevLavId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `#__project_prevobbligatorie`
--
ALTER TABLE `#__project_prevobbligatorie`
  MODIFY `prevObblId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `#__project_prevprivata`
--
ALTER TABLE `#__project_prevprivata`
  MODIFY `prevLavId` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
