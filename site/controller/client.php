<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
// import Joomla controlleradmin library
jimport('joomla.application.component.controller');
/**
 * HelloWorlds Controller
 */
class ProjectControllerClient extends JControllerLegacy {

    public function salva() {
      $itemid = JRequest::getVar('Itemid');

      $model = $this->getModel('client');
      $return = $model->salva();
      if ($return[0]==1) {
          JFactory::getApplication()->enqueueMessage('Salvataggio avvenuto con successo, ora puoi inserire gli interventi.');
          $this->setRedirect('index.php?option='.COMNAME.'&view=client&idClient='.$return[1].'&Itemid='.$itemid);
      }elseif ($return[0]==2) {
          JFactory::getApplication()->enqueueMessage('Salvataggio avvenuto con successo.');
          $this->setRedirect('index.php?option='.COMNAME.'&view=client&idClient='.$return[1].'&Itemid='.$itemid);
      } else {
          JFactory::getApplication()->enqueueMessage('Errore nel salvataggio','error');
          $this->setRedirect(JRoute::_('index.php?option='.COMNAME.'&view=client&idClient=0&Itemid='.$itemid));
      }
    }

	public function generaPdf(){
		$model = $this->getModel('client');
		$model->generaPdf();
	}
	public function delete(){
		$model = $this->getModel('client');
		$itemid = JRequest::getVar('Itemid');
		$model->delete();
		JFactory::getApplication()->enqueueMessage('Errore nel salvataggio','error');
		$this->setRedirect(JRoute::_('index.php?option='.COMNAME.'&view=clients&Itemid='.$itemid));
	}
}
