<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2019 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');


if (strpos(JUri::root(), 'betasiti')) {
    define('COM_MAIL_DEBUG', true);
} else {
    define('COM_MAIL_DEBUG', false);
}

define('VER', (COM_MAIL_DEBUG ? time() : "1.0.1" . time()));
define('COMNAME', 'com_project4life');

JHtml::_('script', 'system/core.js', false, true);
JHtml::_('jquery.framework');
JHtml::_('jquery.ui');

$document = JFactory::getDocument();
$document->addStyleSheet(JURI::base() . 'components/'.COMNAME.'/assets/css/common.css');
$document->addStyleSheet(JURI::base() . 'components/'.COMNAME.'/assets/css/project.css');
$document->addStyleSheet(JURI::base() . 'components/'.COMNAME.'/lib/datetimepicker/bootstrap-datetimepicker.min.css');
$document->addStyleSheet('//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css');
$document->addStyleSheet(JURI::base() . 'components/'.COMNAME.'/lib/chosen-1.8.2/css/chosen.css');
$document->addStyleSheet("https://fonts.googleapis.com/css?family=Roboto:100,300,400,500&display=swap");

$document->addScript(JURI::base() . 'components/'.COMNAME.'/lib/chosen-1.8.2/chosen.jquery.min.js', 'text/javascript');
$document->addScript(JURI::base() . 'components/'.COMNAME.'/lib/datetimepicker/moment.js');
$document->addScript(JURI::base() . 'components/'.COMNAME.'/lib/datetimepicker/bootstrap-datetimepicker.js');
$document->addScript("https://cdn.jsdelivr.net/npm/autonumeric@4.1.0");

if (JFactory::getUser()->guest) {
    $app = JFactory::getApplication();
    $message = "Devi essere loggato per visualizzare il contenuto";
    $url = JRoute::_('index.php?option=com_users&view=login&return=' . base64_encode($_SERVER['REQUEST_URI']));
    $app->redirect($url, $message);
}

// Get an instance of the controller prefixed by HelloWorld
$controller = JControllerLegacy::getInstance('Project');

// Perform the Request task
$input = JFactory::getApplication()->input;
$controller->execute($input->getCmd('task'));

// Redirect if set by the controller
$controller->redirect();