jQuery(document).ready(function ($) {
    jQuery("#date1").datetimepicker({format: 'DD/MM/YYYY'});
    jQuery('#date2').datetimepicker({format: 'DD/MM/YYYY'});
    jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
    });

    /*jQuery('.numeric > input').each(function() {
       // alert($(this).attr('id'));
        new AutoNumeric("#"+$(this).attr('id')).french();
    });*/

   /* jQuery('.numeric input').each(function(){
        $(this).get(0).name = new AutoNumeric($(this).get(0),  'French');
    });*/
   /* jQuery('form').submit(function(){
        var form = $(this);

        jQuery('.numeric input').each(function(i){
            var self = jQuery(this);
            self.val(self.val().split(".").join("").replace("€", "").replace(",", "."));
        });
        return true;
    });*/

    /*.on('dp.change',function (e) {
        var data = jQuery(this).val();
        var diff_date =  jQuery('#dataNascita').val() - data;

        var num_years = diff_date/31536000000;
        var num_months = (diff_date % 31536000000)/2628000000;
alert(data);
        jQuery(this).parent('.calceta').find(".eta").val(num_years+" anni "+num_months+" mesi");
    })*/;

    jQuery(document).on("change", ".addErogsCount", function (e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        var count = jQuery(this).val();
        var alreadyThere = jQuery(this).parents('.erogscontainer').find( ".erogscontent" ).length;
        var totalToAdd = count-alreadyThere;
        var prevId = jQuery(this).attr('data-prevId');
        var prevType = jQuery(this).attr('data-prevType');
        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery(this).parents('.erogscontainer').find( ".erogs" ).append( jQuery( "#addErogs" ).html().split("prevObbli").join(prevType).split("[]").join("["+prevId+"]").split("{}").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(this).parents('.erogscontainer').find( ".erogscontent" ).slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

     jQuery(document).on("change", "#protObbliCount", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#protObbliContainer .protOblliContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#protObbliContainer" ).append( jQuery( "#addProtObbli" ).html().split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#protObbliContainer .protOblliContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", "#protLavCount", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#protLavContainer .protLavContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#protLavContainer" ).append( jQuery( "#addProtLav" ).html().split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#protLavContainer .protLavContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", "#protPrivCount", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#protPrivContainer .protPrivContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#protPrivContainer" ).append( jQuery( "#addProtPriv" ).html().split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#protPrivContainer .protPrivContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", "#tutLavCount", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#tutLavContainer .tutLavContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#tutLavContainer" ).append( jQuery( "#addTut" ).html().split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#tutLavContainer .tutLavContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", "#tutPrivCount", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#tutPrivContainer .tutPrivContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#tutPrivContainer" ).append( jQuery( "#addTut" ).html().split("tutLav").join("tutPriv").split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#tutPrivContainer .tutPrivContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", "#tutCcnlCount", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#tutCcnlContainer .tutCcnlContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#tutCcnlContainer" ).append( jQuery( "#addTut" ).html().split("tutLav").join("tutCcnl").split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#tutCcnlContainer .tutCcnlContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", ".addRenditeCount", function (e) {

        e.stopImmediatePropagation();
        e.stopPropagation();
        var count = jQuery(this).val();
        var alreadyThere = jQuery(this).parents('.renditeContainer').find( ".renditeContent" ).length;
        var totalToAdd = count-alreadyThere;

        var prevId = jQuery(this).attr('data-prevId');
        var prevType = jQuery(this).attr('data-prevType');
        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery(this).parents('.renditeContainer').find( ".rendite" ).append( jQuery( "#addRendita" ).html().split("prevObbli").join(prevType).split("[]").join("["+prevId+"]").split("{}").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(this).parents('.renditeContainer').find( ".renditeContent" ).slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("click", ".submitChiamataForm", function (e) {
        e.stopImmediatePropagation();
        e.stopPropagation();
        $("form[name='clientForm']").submit();
        return false;
    });

    jQuery(document).on("change", "#prevObbli", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#prevObbliContainer .prevOblliContent').length;
        var totalToAdd = count-alreadyThere;

        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#prevObbliContainer" ).append( jQuery( "#addPrevObbli").html().split('--').join(i).split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#prevObbliContainer .prevOblliContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });

        return false;
    });

    jQuery(document).on("change", ".calcann .mens", function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        var mens = jQuery(this).val();
        mens = mens.split(".").join("").replace("€", "").replace(",", ".");

        var ann = mens*13;

        jQuery(this).parents('.calcann').find(".ann").val(ann);
        return false;
    });

    jQuery(document).on("change", ".calcann12 .mens", function (e) {
        e.stopPropagation();
        e.stopImmediatePropagation();
        var mens = jQuery(this).val();
        mens = mens.split(".").join("").replace("€", "").replace(",", ".");

        var ann = mens*12;

        jQuery(this).parents('.calcann12').find(".ann").val(ann);
        return false;
    });
    jQuery(document).on("change", ".calcAnzianita .calcAnz", function (e) {


        var start = jQuery(this).val();
        var type = jQuery(this).attr('data-type');

        switch (type) {
            case 'sett':
                var sett = start;
                var temp = Math.floor(start/52);
                anni = temp+ " anni "+(start-(temp*52))+" settimane";
                var temp2 = Math.floor((start-(temp*52))/4)*4;
                var mesi = (temp*12)+Math.floor((start-(temp*52))/4)+ " mesi "+((start-(temp*52))-temp2)+" settimane";
                break;
            case 'mesi':
                var mesi = start;
                var anni = Math.floor(start/12);
                var sett = Math.floor(anni*52);
                break;
            case 'ann':
                var anni = start;
                var mesi = Math.floor(anni*12);
                var sett = Math.floor(anni*52);
                break;
        }

        jQuery(this).parents('.calcAnzianita').find("[data-type='sett']").val(sett);
        jQuery(this).parents('.calcAnzianita').find("[data-type='mesi']").val(mesi);
        jQuery(this).parents('.calcAnzianita').find("[data-type='ann']").val(anni);
        return false;
    });

    jQuery(document).on("change", "#prevLavoro", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#prevLavoroContainer .prevLavoroContent').length;
        var totalToAdd = count-alreadyThere;
        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#prevLavoroContainer" ).append( jQuery( "#addPrevLavoro" ).html().split('--').join(i).split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#prevLavoroContainer .prevLavoroContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });

    jQuery(document).on("change", "#prevPriv", function (e) {
        var count = jQuery(this).val();
        var alreadyThere = jQuery('#prevPrivContainer .prevPrivContent').length;
        var totalToAdd = count-alreadyThere;
        if (totalToAdd>=0) {
            for (var i = (0+alreadyThere); i < (totalToAdd+alreadyThere); i++) {
                jQuery( "#prevPrivContainer" ).append( jQuery( "#addPrevPriv" ).html().split('--').join(i).split("[]").join("["+i+"]") );
            }
        }  else {
            if (confirm('Questa operazione eliminerà alcuni elementi, vuoi proseguire?')) {
                jQuery(document).find('#prevPrivContainer .prevPrivContent').slice(totalToAdd).remove();
            }
        }
        jQuery(document).find("#clientForm .actChosen").chosen("destroy").chosen();
        chosenAdd();
        jQuery('.datepicker').datetimepicker({
            format: 'DD/MM/YYYY'
        });
        return false;
    });
    function formatNumberInput() {
        jQuery(document).find('input[type=number]').keydown(function (evt) {
            var number = $(this).val().split(".");
            if (number[0].length>3) {

            }
        });

    }
//-------------------------------------------- chosen per le select con aggiunta di nuovi valori (da salvare nel db successivamente -----------------
    jQuery(document).find("#clientForm .actChosen").chosen();
    chosenAdd();
    //var dropDown =  jQuery(document).find(".actChosen");
    function chosenAdd() {
        jQuery(document).find('.chosen-container .chosen-search input[type=text]').keydown(function (evt) {
            //alert('ok');
            var stroke, _ref, target, list;
            var dropDown = $(this).parents('.field ').find('select');
            var newindex = $('option', dropDown).size();
            // get keycode
            //console.log(dropDown.options.length);
            stroke = (_ref = evt.which) != null ? _ref : evt.keyCode;
            //console.log(dropDown.getAttribute("name"));
            target = $(evt.target);
            // get the list of current options
            //list = $(this).map(function () { return $(this).text(); }).get();
            list = target.parents('.chosen-container').find('.chosen-results li').map(function () {
                return $(this).text();
            }).get()

            if (stroke === 9 || stroke === 13) {

                var value = $.trim(target.val());
                // if the option does not exists
                var sel = $.inArray(value, list);
                if (sel < 0) {
                    var option = $('<option>');
                    option.text(value).val(value).appendTo(dropDown);
                    option.attr('selected', 'selected');
                    // add the option and set as selected
                    dropDown.trigger("chosen:updated");
                    if (dropDown.hasClass('nomeCassa')) {
                        $('.nomeCassa').each(function (i) {
                            var option2 = $('<option>');
                            option2.text(value).val(value).appendTo($(this));
                            $(this).trigger("chosen:updated");
                        });
                    }
                    if (dropDown.hasClass('tipoRendita')) {
                        $('.tipoRendita').each(function (i) {
                            var option2 = $('<option>');
                            option2.text(value).val(value).appendTo($(this));
                            $(this).trigger("chosen:updated");
                        });
                    }
                    if (dropDown.hasClass('tipoErog')) {
                        $('.tipoErog').each(function (i) {
                            var option2 = $('<option>');
                            option2.text(value).val(value).appendTo($(this));
                            $(this).trigger("chosen:updated");
                        });
                    }
                    if (dropDown.hasClass('modalita')) {
                        $('.modalita').each(function (i) {
                            var option2 = $('<option>');
                            option2.text(value).val(value).appendTo($(this));
                            $(this).trigger("chosen:updated");
                        });
                    }
                    return true;
                }
                // trigger the update event

            }
        });

    }
});