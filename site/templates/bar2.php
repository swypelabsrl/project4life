<?php
require_once(dirname(__FILE__) . '/../lib/SVGGraph/autoloader.php');
$datas = unserialize($_GET['obblig']);
$max = $_GET['max'];

$ral = $_GET['ral'];
/*$ral2 = $ral;
foreach($values as $cassa) {
	$settings['legend_entries'][] = $cassa->nomeCassa;
	foreach($cassa as $erogs) {
		if($erogs->showinpdf) {
			if($erogs->viewPDF==1) {
				$ral2 = $ral/12;
			}
			$data[$erogs->eta][] = array('data' => $erogs->data, 'valore' => $erogs->valore);
		}
	}
}

$ral= $ral2;*/

$settings = array(
	'back_colour' => '#eee',
	'stroke_colour' => '#000',
	'back_stroke_width' => 0,
	'back_stroke_colour' => '#eee',
	'axis_colour' => '#333',
	'axis_overlap' => 2,
	'axis_font' => 'Georgia',
	'axis_font_size' => 10,
	'grid_colour' => '#666',
	'label_colour' => '#000',
	'pad_right' => 20,
	'pad_left' => 20,
	'minimum_grid_spacing' => 20,
	'legend_position' => "outer right -5 40",
	'legend_stroke_width' => 0,
	'legend_shadow_opacity' => 0,
	'legend_title' => "Legend",
	'legend_text_side' => "left",
	'stack_group'=>array(1, 2, 3),
	'show_grid' => true,
	'show_grid_h' =>true,
	'show_grid_v' =>false,
	'bar_total_round' =>50,
	'back_colour' => '#ffffff',
	'stroke_width' => 0,
	'grid_subdivision_colour'=>'#eee',
	'bar_space' => 0,
	'axis_max_v' => $max,
	'keep_colour_order'=>true
);

	$values2[] = array('da oggi a '.key($data)=>$ral);

$count = count($data)-1;
$i=0;
$keys = array_keys($data);

/*foreach($data as $key=>$array) {

	if($i<$count) {
		$datas['da ' . $key . ' a ' .$keys[$i+1]] = $array;
	} else {
		$datas['da ' . $key . ' in poi'] = $array;
	}
	$i++;
}*/

/*foreach($datas as $key=>$array) {
	foreach($array as $val) {
		$values2[] = array($key=>$val->valore);
	}
}*/


foreach($datas as $key=>$data) {
	$values[]['test'] = $data;
}
/*$values = array(
	array('Dough' => 30, 'Ray' => 50, 'Me' => 40, 'So' => 25, 'Far' => 45, 'Lard' => 35),
	array('Dough' => 20, 'Ray' => 30, 'Me' => 20, 'So' => 15, 'Far' => 25, 'Lard' => 35,
		'Tea' => 45)
);*/
//var_dump($values);


if(isset($_GET['prevtype'])) {
	switch ($_GET['prevtype']) {
		case 1:
			//colori sul rosso per previdenza obbligatoria
			$colours = array('#FFAF4B','#FF5C5C','#f8b500','#FF670F','#fac695');
			break;
		case 2:
			//colori sul verde per previdenza lavoro
			$colours = array('#a9db80','#52B152','#61c419','#005700','#7cbc0a');
			break;
		case 3:
			//colori sul azzurro per previdenza privata
			$colours = array('#e4f5fc','#21b4e2','#E0F3FA','#9EE8FA','#B6DFFD');
			break;
	}

} else {
	$colours = array('#FFAF4B','#a9db80','#e4f5fc','#F15D5B');
}


$graph = new Goat1000\SVGGraph\SVGGraph(650, 350, $settings);

$graph->colours($colours);
$graph->values($values);

$graph->render('StackedBarGraph');
?>