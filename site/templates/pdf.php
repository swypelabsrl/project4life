<?php
$colours = array(
	'#FFAF4B','#a9db80','#e4f5fc','#F15D5B'
);
$mainColor = '#4EC2C1';
?>
<html>
	<body style="font-size:11px !important;">
		<!--instestazione-->

		<table style="width: 100%">
			<tr>
				<td style="height:10px;">
				</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
				<td style="width: 60%">
					<img src="/components/com_project4life/assets/images/logo.jpg" />
				</td>
				<td style="width: 40%">
					<table style="width: 100%">
						<tr>
							<td style="width: 100%; text-align: right;">
								<?php echo $client->luogo; ?>
							</td>
						</tr>
						<tr>
							<td style="width: 100%; text-align: right;">
								<?php echo date("d/m/Y",strtotime($client->data)); ?>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
				<td style="height:10px;">
				</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
				<td>
					<div style="height:1px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
				</td>
			</tr>
		</table>

		<!--dati anagrafici cliente-->
		<table style="width: 100%">
			<tr>
				<td>
					<h2 style="font-size:13px;">CLIENTE</h2>
				</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
				<td style="height:10px;">
				</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr style="height:40px">
				<td style="width: 20%; height:20px; font-weight:bold;font-family:'montserratsemibold">
					<b>Nome e Cognome</b>
				</td>
				<td style="width: 80%;">
					<?php echo $client->nome. " " . $client->cognome; ?>
				</td>
			</tr>
			<tr style="height:40px">
				<td style="width: 20%; height:20px; font-weight:bold;">
					<strong>Età Raggiunta</strong>
				</td>
				<td style="width: 80%;">
					<?php echo $client->etaRaggiunta; ?>
				</td>
			</tr>
			<tr style="height:40px">
				<td style="width: 20%; height:20px; font-weight:bold;">
					<strong>Professione</strong>
				</td>
				<td style="width: 80%;">
					<?php echo $client->professioneName; ?>
				</td>
			</tr>
		</table>
		<table style="width: 100%">
			<tr>
				<td style="height:10px;">
				</td>
			</tr>
		</table>
		<table style="width:100%; background-color: #ffffff;">
			<tr>
				<td colspan="2">
					<div style="height:5px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
				</td>
			</tr>
		</table>
		<h2 style="font-size:13px;font-weight:bold;">MONTANTE CONTRIBUTIVO</h2>
		<table style="width:100%; background-color: #ffffff;">

			<tr>
				<td style="text-align: center;width:60%">
					<img src="<?php echo JURI::root(); ?>/components/com_project4life/templates/pie.php?obbligvalues=<?php echo $totalPrev; ?>&dim=250.250" alt="" width="250px" height="250px">
				</td>
				<td style="text-align: left; vertical-align: text-top;width:40%">
					<table width="100%">
					<?php
					$totalPrev =unserialize(urldecode($totalPrev));
					$total = 0;
					foreach($totalPrev as $prev) {
						$total = $prev+$total;
					}
					?>
						<tr>
							<td style="width:220px; text-align:right;height:30px;font-size:10px;" colspan="4">
								<span style="font-weight:bold;height:30px; text-align:right;">Totale: <?php echo number_format($total, 0, ",", ".")." €"; ?></span>
							</td>
						</tr>

					<?php

					$k=0;
					foreach($totalPrev as $key=>$prev) {
						$perc = round(($prev*100)/$total,0);
						//if($perc>0) {
							?>

								<tr>
									<td style="width:15px">
										<div style="width:15px;height:15px;background-color: <?php echo $colours[$k]; ?>;border-radius: 50%;">
											&nbsp;
										</div>
									</td>
									<td style="width:5px">
									</td>
									<td style="text-align:left;width:120px; font-size:10px;">
										<?php echo $key; ?>
									</td>
									<td style="text-align:right;width:80px; font-size:10px;">
										<?php echo $perc . "% - " . number_format($prev, 0, ",", ".")  . " €"; ?>
									</td>
								</tr>

							<?php
							$k++;
						//}
					}

					?>
					</table>
				</td>
			</tr>
		</table>

		<br clear="all"/>
		<table style="widrth:100%">
			<tr>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>
		<table style="width:100%">
			<tr style="">

				<td>&nbsp;</td>
			</tr>
			<tr style="">

				<?php

				foreach($previdenze as $key=>$previdenza) {
					$totalPrev = unserialize(urldecode($previdenza));

					if (count($totalPrev) == 1 && $totalPrev[""] == "0") {
					} else { ?>
						<td style="text-align: center; padding:20px 0; vertical-align: text-top;width:220px; height:35px;border-top:1px solid #eee;border-left:1px solid #eee;border-right:1px solid #eee;">
							<h2 style="font-size:13px;"><?php echo strtoupper($key); ?></h2>
						</td>
					<?php }
				} ?>
			</tr>
			<tr>
				<?php
				$total2 = 0;
				foreach($previdenze as $key=>$previdenza) {
					$totalPrev = unserialize(urldecode($previdenza));
					if($key=="Previdenza Obbligatoria") {
						$colours = array('#FFAF4B','#FF5C5C','#f8b500','#FF670F','#fac695');
					} else if($key=="Previdenza Aziendale") {
						$colours = array('#a9db80','#52B152','#61c419','#005700','#7cbc0a');
					} else {
						$colours = array('#e4f5fc','#21b4e2','#E0F3FA','#9EE8FA','#B6DFFD');
					}
					if(count($totalPrev)==1 && $totalPrev[""]=="0") { } else {
						?>
						<td style="text-align: left; vertical-align: text-top;width:220px;border-left:1px solid #eee;border-right:1px solid #eee;padding:20px 0;">
							<?php

							$total = 0;
							foreach ($totalPrev as $prev) {
								$total = $prev + $total;
								$total2 = $prev+$total2;
							}

							$k = 0;
							foreach ($totalPrev as $key => $prev) {
								$perc = round(($prev * 100) / $total, 0);
								//if ($perc > 0) {
									?>
									<table width="200px">
										<tr>
											<td style="width:15px">
												<div style="width:15px;height:15px;background-color: <?php echo $colours[$k]; ?>;border-radius: 50%;">
													&nbsp;
												</div>
											</td>
											<td style="width:5px">
											</td>
											<td style="text-align:left;width:105px; font-size:10px;">
												<?php echo $key; ?>
											</td>
											<td style="text-align:right;width:80px; font-size:10px;">
												<?php echo $perc . "% - " . number_format($prev, 0, ",", ".")  . " €"; ?>
											</td>

										</tr>
									</table>
									<?php
									$k++;
								//}
							}
							?>
						</td>
					<?php }
				}
				?>

			</tr>
			<tr>
				<?php

				foreach($previdenze as $key=>$previdenza) {
					$totalPrev = unserialize(urldecode($previdenza));


						if ($key == "Previdenza Obbligatoria") {
							$type = 1;
						} else if ($key == "Previdenza Aziendale") {
							$type = 2;
						} else {
							$type = 3;
						}

						?>
						<td style="border-bottom:1px solid #eee;border-left:1px solid #eee;border-right:1px solid #eee;text-align:center;">
							<?php
							if ($totalPrev) {
								?>
							<img src="<?php echo JURI::root(); ?>/components/com_project4life/templates/bar2.php?obblig=<?php echo $previdenza; ?>&dim=200.200&prevtype=<?php echo $type; ?>&max=<?php echo $total2;?>" alt="" width="200px" height="200px">
							<?php } ?>
						</td>
						<?php
					}

				?>
			</tr>
		</table>

		<br clear="all" />
		<br pagebreak="true"/>
		<?php $previdenze = array('Previdenza Obbligatoria'=>$erogObbli,'Previdenza Aziendale'=>$erogLav,'Previdenza Privata'=>$erogPriv);
		//$previdenzeChart = array('Obbligatoria'=>urlencode(serialize($erogObbli)),'Lavoro'=>urlencode(serialize($erogLav)),'Privata'=>urlencode(serialize($erogPriv))); ?>

		<?php

		$colours = array('#6accc9', '#a1d54f','#478b21','#FCCD4D','#F26060','#F26060');
		foreach($previdenze as $key=>$previdenza) {
			if (count($previdenza) > 0) {
				?>


					<h2 style="font-size:13px"><?php echo strtoupper($key); ?></h2>
				<table style="width: 100%">
					<tr>
						<td>
							<div style="height:5px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
						</td>
					</tr>
				</table>
					<?php
					$ral2= $client->ral;
					$mensann='annuale';
					foreach ($previdenza as $key2=>$cassa) { ?>
						<table style="width: 100%;" cellpadding="5px" cellspacing="0">
							<tr style="background-color:#fcfcfc;">
								<th style="width: 20%;border:1px solid #eee; text-align:center; font-weight:bold;">
									<?php echo $cassa[0]->nomeCassa; ?>
								</th>
								<th style="width: 20%;border:1px solid #eee; text-align:center; font-weight:bold;vertical-align: center">
									<br/>Valore Rendita già acquisita
								</th>
								<?php $width = floor(60 / count($cassa));
								foreach ($cassa as $erog) { ?>
									<th style="width: <?php echo $width; ?>%;border:1px solid #eee; text-align:center; font-weight:bold;">
										<?php echo $erog->erogName; ?><br/>
										<?php echo date("d/m/Y", strtotime($erog->data)); ?>
										<br/>
										<?php echo $erog->eta; ?>
									</th>
								<?php } ?>
							</tr>
							<tr style="height:40px">
								<td style="width: 20%;border:1px solid #eee; font-weight:bold;">
									Cessazione Contribuzione
								</td>
								<td style="width: 20%;border:1px solid #eee; text-align: right;">
									<?php if($cassa[0]->viewPDF==1) { ?>
										€ <?php echo number_format($erog->renditaAcquisitaMens,0,",","."); ?>
										<?php
									} else {
										?>
										€ <?php echo number_format($erog->renditaAcquisitaAnn, 0, ",", "."); ?>
										<?php
									}
									?>

								</td>
								<?php foreach ($cassa as $erog) { ?>
									<td style="width: <?php echo $width; ?>%;border:1px solid #eee; text-align: right;">
										<?php if($erog->viewPDF==1) { ?>
											€ <?php echo number_format($erog->renditaAcquisitaMens,0,",","."); ?>
											<?php
										} else {
											?>
												€ <?php echo number_format($erog->renditaAcquisitaAnn, 0, ",", "."); ?>
												<?php
										}
										?>

									</td>
								<?php } ?>
							</tr>
							<tr style="background-color:#fcfcfc;">
								<td style="width: 20%;border:1px solid #eee; font-weight:bold;">
									RAL <?php echo number_format($erog->RAL,0,",","."); ?>
								</td>
								<td style="width: 20%;border:1px solid #eee; text-align: right;">
									<?php if($cassa[0]->viewPDF==1) { ?>
										€ <?php echo number_format($erog->renditaAcquisitaMens,0,",","."); ?>
										<?php
									} else {
										?>
										€ <?php echo number_format($erog->renditaAcquisitaAnn, 0, ",", "."); ?>
										<?php
									}
									?>
								</td>
								<?php foreach ($cassa as $key5=>$erog) { ?>
									<td style="width: <?php echo $width; ?>%;border:1px solid #eee; text-align: right;">
										<?php if($erog->viewPDF==1) { ?>
											€ <?php echo number_format($erog->valore,0,",","."); ?>
											<?php
										} else {
												?>
												€ <?php echo number_format($erog->valore, 0, ",", "."); ?>
												<?php
										}
										?>
									</td>
								<?php
									if($erog->showinpdf) {
										if($erog->viewPDF==1) {
											if($key=='Previdenza Obbligatoria') {
												$ral2 = $client->ral / 13;
												$erog->RAL = $erog->RAL / 13;
											} else {
												$ral2 = $client->ral / 13;
												$erog->RAL = 0;
											}
											$mensann='mensile';

										}
										$data[$erog->eta][] = array('data' => $erog->data, 'valore' => $erog->valore,'ral'=>$erog->RAL);
										$data2[$erog->eta][] = array('cassa' => $erog->nomeCassa, 'valore' => $erog->valore,'ral'=>$erog->RAL);
									}
								}
								?>
							</tr>
						</table>
						<br/><br/>
						<table style="width: 100%">
							<tr>
								<td>
									<div style="height:1px;background-color: #eee;font-size:1px;">&nbsp;</div>
								</td>
							</tr>
						</table>
						<br/>


					<?php } ?>
				<?php
			}
			?>

		<?php
		}
		?>
		<br pagebreak="true"/>
		<h2 style="font-size:13px">PROIEZIONE DEI REDDITI NEL TEMPO (<?php echo $mensann;?>)</h2>
		<table style="width: 100%">
			<tr>
				<td>
					<div style="height:5px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
				</td>
			</tr>
		</table>
		<!--<table width="215px">
			<tr>
				<td style="width:15px">
					<div style="width:15px;height:15px;background-color: #4DC1C0;border-radius: 50%;">
						&nbsp;
					</div>
				</td>
				<td style="width:5px">
				</td>
				<td style="text-align:left;width:120px; font-size:10px;">
					RAL
				</td>
				<td style="text-align:right;width:80px; font-size:10px;">
					<?php /*echo number_format($ral2,0,",","."); */?> €
				</td>

			</tr>
		</table>-->

		<?php
		$k=0;
		$i =0;
		$totalRal=0;
		foreach($data2 as $dat) {

			$count = count($dat);

// Print array elements before sorting

			for ($i = 0; $i < $count; $i++) {
				for ($j = $i + 1; $j < $count; $j++) {
					if ($dat[$i]["valore"] < $dat[$j]["valore"]) {
						$temp = $dat[$i];
						$dat[$i] = $dat[$j];
						$dat[$j] = $temp;
					}
				}
			}
?>
			<table>
				<tr>
					<td style="width:70px"></td>
					<td>


			<?php
			foreach($dat as $d) {
				if($d['ral']>0) {
					?>
					<table width="215px">
						<tr>
							<td style="width:15px">
								<div style="width:15px;height:15px;background-color: <?php echo $colours[$k]; ?>;border-radius: 50%;">
									&nbsp;
								</div>
							</td>
							<td style="width:5px">
							</td>
							<td style="text-align:left;width:120px; font-size:10px;">
								RAL <?php echo $d['cassa']; ?>
							</td>
							<td style="text-align:right;width:80px; font-size:10px;">
								<?php echo number_format($d['ral'], 0, ",", ".") . " €"; ?>
							</td>

						</tr>
					</table>
					<?php
					$totalRal = $totalRal + $d['ral'];
					$k++;
				}
			}
		}
?>
						<table width="215px">
							<tr>
								<td style="width:15px; border-top: 1px solid #000;">
								</td>
								<td style="width:5px; border-top: 1px solid #000;">
								</td>
								<td style="text-align:left;width:120px; font-size:12px; font-weight:bold; border-top: 1px solid #000;">

								</td>
								<td style="text-align:right;width:80px; font-size:12px; font-weight:bold; border-top: 1px solid #000; padding:10px 0 0">

								</td>
							</tr>
							<tr>
								<td style="width:15px;">
								</td>
								<td style="width:5px;">
								</td>
								<td style="text-align:left;width:120px; font-size:12px; font-weight:bold;">
									Totale:
								</td>
								<td style="text-align:right;width:80px; font-size:12px; font-weight:bold; padding:10px 0 0">
									<?php echo number_format($totalRal, 0, ",", ".")  . " €"; ?>
								</td>
							</tr>
						</table>
					</td>
					<td style="width:125px"></td>
					<td>
		<?php
		$totalCasse = 0;
		foreach($data2 as $dat) {
			for ($i = 0; $i < $count; $i++) {
				for ($j = $i + 1; $j < $count; $j++) {
					if ($dat[$i]["valore"] < $dat[$j]["valore"]) {
						$temp = $dat[$i];
						$dat[$i] = $dat[$j];
						$dat[$j] = $temp;
					}
				}
			}
			foreach($dat as $d) {
			?>
			<table width="215px">
				<tr>
					<td style="width:15px">
						<div style="width:15px;height:15px;background-color: <?php echo $colours[$k]; ?>;border-radius: 50%;">
							&nbsp;
						</div>
					</td>
					<td style="width:5px">
					</td>
					<td style="text-align:left;width:120px; font-size:10px;">
						<?php echo $d['cassa']; ?>
					</td>
					<td style="text-align:right;width:80px; font-size:10px;">
						<?php echo number_format($d['valore'], 0, ",", ".")  . " €"; ?>
					</td>

				</tr>
			</table>
			<?php
				$totalCasse = $totalCasse + $d['valore'];
			$k++;
			}
		}

		?>
						<table width="215px">
							<tr>
								<td style="width:15px; border-top: 1px solid #000;">
								</td>
								<td style="width:5px border-top: 1px solid #000;">
								</td>
								<td style="text-align:left;width:120px; font-size:12px; font-weight:bold; border-top: 1px solid #000;">

								</td>
								<td style="text-align:right;width:80px; font-size:12px; font-weight:bold; border-top: 1px solid #000;">

								</td>
							</tr>
							<tr>
								<td style="width:15px;">
								</td>
								<td style="width:5px">
								</td>
								<td style="text-align:left;width:120px; font-size:12px; font-weight:bold;">
									Totale:
								</td>
								<td style="text-align:right;width:80px; font-size:12px; font-weight:bold;">
									<?php echo number_format($totalCasse, 0, ",", ".")  . " €"; ?>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<table style="width: 100%">
				<tr>
					<td>
						<img src="<?php echo JURI::root(); ?>components/com_project4life/templates/bar.php?obblig=<?php echo urlencode(json_encode($data)); ?>&ral=<?php echo $ral2; ?>" alt="" width="650px" height="350px">
					</td>
				</tr>
			</table>

		<br pagebreak="true"/>
		<?php if($client->AnalisiEstrattoConto && $client->analisiInPDF) { ?>
			<h2 style="font-size:13px;">ANALISI ESTRATTO CONTO CONTRIBUTIVO</h2>
			<table style="width: 100%">
				<tr>
					<td>
						<div style="height:5px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
					</td>
				</tr>

				<tr>
					<td style="width: 100%">
						<?php echo $client->AnalisiEstrattoConto; ?>
					</td>
				</tr>
			</table>
			<br/><br/>
		<?php } ?>
		<?php if($client->analisi && $client->diagnosiInPDF) { ?>
			<h2 style="font-size:13px;">PRINCIPALI EVIDENZE</h2>
		<table style="width: 100%">
			<tr>
				<td>
					<div style="height:5px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
				</td>
			</tr>
			<tr>
				<td style="width: 100%">
					<?php echo $client->analisi; ?>
				</td>
			</tr>
		</table>

		<br/><br/>
		<?php } ?>
		<?php if($client->conclusioni && $client->conclusioniInPDF) { ?>
			<h2 style="font-size:13px;">PROSSIMI PASSI</h2>
			<table style="width: 100%">

			<tr>
				<td>
					<div style="height:5px;background-color: <?php echo $mainColor; ?>;font-size:1px;">&nbsp;</div>
				</td>
			</tr>
			<tr>
				<td style="width: 100%">
					<?php echo $client->conclusioni; ?>
				</td>
			</tr>
		</table>
		<?php } ?>

	</body>
</html>