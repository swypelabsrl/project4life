<?php
$obbligvalues = unserialize(urldecode($_GET['obbligvalues']));
if(isset($_GET['dim'])) {
	$dim =explode(".",$_GET['dim']);
} else {
	$dim = array(450,450);
}
if(isset($_GET['bg'])) {
	$bg = $_GET['bg'];
} else {
	$bg = 'ffffff';
}

$settings = array(
	'label_font_size' => 40,
	'show_labels' => false,
	'show_label_amount' => false,
	'show_label_percent' =>false,
	'aspect_ratio' => '1.0',
	'back_colour' => "#".$bg,
	'back_stroke_width' => 0,
	'start_angle'=>-90,
	'inner_radius' => 0.3,
	'sort' => false,
	'stroke_width' => 0
);

if(isset($_GET['prevtype'])) {
	switch ($_GET['prevtype']) {
		case 1:
			//colori sul rosso per previdenza obbligatoria
			$colours = array('#FFAF4B','#FF5C5C','#f8b500','#FF670F','#fac695');
			break;
		case 2:
			//colori sul verde per previdenza lavoro
			$colours = array('#a9db80','#52B152','#61c419','#005700','#7cbc0a');
			break;
		case 3:
			//colori sul azzurro per previdenza privata
			$colours = array('#e4f5fc','#21b4e2','#E0F3FA','#9EE8FA','#B6DFFD');
			break;
	}

} else {
	$colours = array('#FFAF4B','#a9db80','#e4f5fc','#F15D5B');
}


//var_dump(unserialize($obbligvalues));die();
require_once(dirname(__FILE__) . '/../lib/SVGGraph/autoloader.php');
$graph = new Goat1000\SVGGraph\SVGGraph($dim[0],$dim[1],$settings);
$graph->colours($colours);
$graph->values($obbligvalues);
$graph->render('DonutGraph');
?>