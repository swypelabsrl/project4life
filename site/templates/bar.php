<?php
require_once(dirname(__FILE__) . '/../lib/SVGGraph/autoloader.php');
$data = json_decode($_GET['obblig']);

$ral = $_GET['ral'];

$settings = array(
	'back_colour' => '#eee',
	'stroke_colour' => '#000',
	'back_stroke_width' => 0,
	'back_stroke_colour' => '#eee',
	'axis_colour' => '#333',
	'axis_overlap' => 2,
	'axis_font' => 'Georgia',
	'axis_font_size' => 10,
	'grid_colour' => '#666',
	'label_colour' => '#000',
	'pad_right' => 20,
	'pad_left' => 20,
	'minimum_grid_spacing' => 20,
	'legend_position' => "outer right -5 40",
	'legend_stroke_width' => 0,
	'legend_shadow_opacity' => 0,
	'legend_title' => "Legend",
	'legend_text_side' => "left",
	'stack_group'=>array(1, 2, 3),
	'show_grid' => true,
	'show_grid_h' =>true,
	'show_grid_v' =>false,
	'bar_total_round' =>50,
	'back_colour' => '#ffffff',
	'stroke_width' => 0,
	'grid_subdivision_colour'=>'#eee',
	'bar_space' => 0,
	'grid_division_h' => 2,
	'show_axis_text_h' => false
);

$settings['label'][] = array(
	330, 355,
	"Da ".key($data)." in poi ",
	'angle' => 0,
	'position' => 'top'
);
$settings['label'][] = array(
	60, 352,
	"Oggi",
	'angle' => 0,
	'position' => 'top'
);

$count = count($data)-1;
$i=0;
$keys = array_keys($data);

foreach($data as $key=>$array) {
		$datas['oggi'] = $array;
}

foreach($datas as $key=>$array) {
	foreach($array as $val) {
		if($val->ral>0) {
			$values2[] = array($key => $val->ral);
		}
	}
}

$datas=array();

//$values2[] = array('da oggi a '.key($data)=>$ral);

foreach($data as $key=>$array) {
	if($i<$count) {
		$datas['da ' . $key . ' a ' .$keys[$i+1]] = $array;
	} else {
		$datas['da ' . $key . ' in poi'] = $array;
	}
	$i++;
}

foreach($datas as $key=>$array) {
	foreach($array as $val) {
		if($val->valore>0) {
			$values2[] = array($key => $val->valore);
		}
	}
}
$count = count($values2);

// Print array elements before sorting

for ($i = 0; $i < $count; $i++) {
	for ($j = $i + 1; $j < $count; $j++) {
		if ($values2[$i] < $values2[$j]) {
			$temp = $values2[$i];
			$values2[$i] = $values2[$j];
			$values2[$j] = $temp;
		}
	}
}


//var_dump($values2);die();
$colours = array('#6accc9', '#a1d54f','#478b21','#FCCD4D','#F26060','#F26060');


$graph = new Goat1000\SVGGraph\SVGGraph(650, 350, $settings);

$graph->colours($colours);
$graph->values($values2);

$graph->render('StackedBarGraph');
?>