<?php

/**
 * @package     Joomla.Administrator
 * @subpackage  com_helloworld
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * HelloWorld Model
 *
 * @since  0.0.1
 */
class ProjectModelClients extends JModelList {

    public function __construct($config = array()) {
        $config['filter_fields'] = array(
            'ordering',
            'search', 'status', 'type', 'comune', 'filter_manutentore', 'data'
        );
        parent::__construct($config);

        $mainframe = JFactory::getApplication();
        $this->limit = $mainframe->getUserStateFromRequest('global.list.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
        $this->limit = $this->limit ? $this->limit : 25;
        $this->limitstart = JRequest::getVar('limitstart', 0, '', 'int');
        $this->limitstart = ($this->limit != 0 ? (floor($this->limitstart / $this->limit) * $this->limit) : 0);

        $this->setState('limit', $this->limit);
        $this->setState('limitstart', $this->limitstart);
    }

    protected function populateState($ordering = null, $direction = null) {
        $app = JFactory::getApplication();

        $this->setState('filter.search', $app->getUserStateFromRequest($this->context . ".filter.search", "filter_search"));
        $this->setState('filter.status', $app->getUserStateFromRequest($this->context . ".filter.status", "filter_status"));
        $this->setState('filter.type', $app->getUserStateFromRequest($this->context . ".filter.type", "filter_type"));
        $this->setState('filter.tipoChiamata', $app->getUserStateFromRequest($this->context . ".filter.tipoChiamata", "filter_chiamata"));
        $this->setState('filter.comune', $app->getUserStateFromRequest($this->context . ".filter.comune", "filter_comune"));
        $this->setState('filter.scadute', $app->getUserStateFromRequest($this->context . ".filter.scadute", "filter_scadute"));
        $this->setState('filter.incorso', $app->getUserStateFromRequest($this->context . ".filter.incorso", "filter_incorso"));
        $this->setState('filter.data', $app->getUserStateFromRequest($this->context . ".filter.data", "filter_data"));
        $this->setState('filter.manutentore', $app->getUserStateFromRequest($this->context . ".filter.manutentore", "filter_manutentore"));
    }

    public function getPagination() {
        $jpagination = new JPagination($this->getTotal(), $this->limitstart, $this->limit);

        return $jpagination;
    }

    public function getItems() {
        $db = JFactory::getDBO();
        $query = $this->costruisciQuery();

        $db->setQuery($query, $this->limitstart, $this->limit);

        $results = $db->loadObjectList();



        return $results;
    }


    public function getTotal() {
        $db = JFactory::getDBO();
        $query = $this->costruisciQuery();
        $db->setQuery($query);
        $db->query();
        $results = $db->getNumRows();
        return $results;
    }

    public function costruisciQuery() {
        //$filterCompanyName = JRequest::getVar('filterCompanyName');

        $db = JFactory::getDBO();
        $query = $db->getQuery(true);

        $search = $db->escape($this->getState('filter.search'));
        $status = $db->escape($this->getState('filter.status'));
        $type = $db->escape($this->getState('filter.type'));
        $comune = $db->escape($this->getState('filter.comune'));
        $man = $db->escape($this->getState('filter.manutentore'));
        $data = $db->escape($this->getState('filter.data'));
        $scadute = $db->escape($this->getState('filter.scadute'));
        $incorso = $db->escape($this->getState('filter.incorso'));
        $tipoChiamata = $db->escape($this->getState('filter.tipoChiamata'));

        $daconsuntivare = 0;

        if ($status == 7) {
            $daconsuntivare = 1;
            $status = null;
        }

        $user = JFactory::getUser();
        $userid = $user->get('id');

        $query->select("*")
                ->from($db->qn("#__project_clients") . ' AS c')
	        ->where('deleted!=1');



        return $query;
    }



}
