<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modelitem');

class ProjectModelCLient extends JModelItem {

    public function getItem() {
        $db = JFactory::getDbo();
        $idClient = JRequest::getVar('idClient');
        if ($idClient) {
            $query = "
                SELECT c.*
                FROM #__project_clients AS c
                WHERE idClient = " . $idClient;
            $db->setQuery($query);
            $chiamata = $db->loadObject();

            $query = "
                SELECT c.*
                FROM #__project_prevobbligatorie AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

            $db->setQuery($query);
            $chiamata->prevObbliArray = $db->loadObjectList();

            foreach($chiamata->prevObbliArray as $key=>$prev) {

                $query = $db->getQuery(true);
                $query->select('*');
                $query->from($db->quoteName('#__project_erog_prev_obblig'));
                $query->where($db->quoteName('prevObblId') . ' = '. $prev->prevObblId);
                $query->where($db->quoteName('idClient') . ' = '. $idClient);
                $query->order($db->quoteName('orders'));
                $db->setQuery($query);
                $chiamata->prevObbliArray[$key]->erogResults = $db->loadObjectList();
            }

            $query = "
                SELECT c.*
                FROM #__project_prevlavoro AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";
            $db->setQuery($query);
            $chiamata->prevLavoroArray = $db->loadObjectList();

            foreach($chiamata->prevLavoroArray as $key=>$prev) {

                $query = $db->getQuery(true);
                $query->select('*');
                $query->from($db->quoteName('#__project_erog_prev_lavoro'));
                $query->where($db->quoteName('prevLavId') . ' = '. $prev->prevLavId);
	            $query->where($db->quoteName('idClient') . ' = '. $idClient);
                $query->order($db->quoteName('orders'));
                $db->setQuery($query);
                $chiamata->prevLavoroArray[$key]->erogResults = $db->loadObjectList();

            }

	        foreach($chiamata->prevLavoroArray as $key=>$prev) {

		        $query = $db->getQuery(true);
		        $query->select('*');
		        $query->from($db->quoteName('#__project_erog_prev_lavoro_rendite'));
		        $query->where($db->quoteName('prevLavId') . ' = '. $prev->prevLavId);
		        $query->where($db->quoteName('idClient') . ' = '. $idClient);
		        $query->order($db->quoteName('orders'));
		        $db->setQuery($query);
		        $chiamata->prevLavoroArray[$key]->renditeResults = $db->loadObjectList();

	        }

            $query = "
                SELECT c.*
                FROM #__project_prevprivata AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";
            $db->setQuery($query);
            $chiamata->prevPrivArray = $db->loadObjectList();

            foreach($chiamata->prevPrivArray as $key=>$prev) {
                $query = $db->getQuery(true);
                $query->select('*');
                $query->from($db->quoteName('#__project_erog_prev_privata'));
                $query->where($db->quoteName('prevPrivId') . ' = '. $prev->prevPrivId);
	            $query->where($db->quoteName('idClient') . ' = '. $idClient);
                $query->order($db->quoteName('orders'));
                $db->setQuery($query);
                $chiamata->prevPrivArray[$key]->erogResults = $db->loadObjectList();
            }

	        foreach($chiamata->prevPrivArray as $key=>$prev) {
		        $query = $db->getQuery(true);
		        $query->select('*');
		        $query->from($db->quoteName('#__project_erog_prev_privata_rendite'));
		        $query->where($db->quoteName('prevPrivId') . ' = '. $prev->prevPrivId);
		        $query->where($db->quoteName('idClient') . ' = '. $idClient);
		        $query->order($db->quoteName('orders'));
		        $db->setQuery($query);
		        $chiamata->prevPrivArray[$key]->renditeResults = $db->loadObjectList();
	        }

	        $query = "
                SELECT c.*
                FROM #__project_protobbligatorie AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

	        $db->setQuery($query);
	        $chiamata->protObbliArray = $db->loadObjectList();

	        $query = "
                SELECT c.*
                FROM #__project_protlavoro AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

	        $db->setQuery($query);
	        $chiamata->protLavArray = $db->loadObjectList();

	        $query = "
                SELECT c.*
                FROM #__project_protprivata AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

	        $db->setQuery($query);
	        $chiamata->protPrivArray = $db->loadObjectList();

	        $query = "
                SELECT c.*
                FROM #__project_tutlavoro AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

	        $db->setQuery($query);
	        $chiamata->tutLavArray = $db->loadObjectList();

	        $query = "
                SELECT c.*
                FROM #__project_tutprivata AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

	        $db->setQuery($query);
	        $chiamata->tutPrivArray = $db->loadObjectList();

	        $query = "
                SELECT c.*
                FROM #__project_tutccnl AS c
                WHERE idClient = " . $idClient."
                ORDER BY orders";

	        $db->setQuery($query);
	        $chiamata->tutCcnlArray = $db->loadObjectList();

        }

        return $chiamata;
    }

    public function salva()
    {

	    $user = JFactory::getUser();
	    $db = JFactory::getDBO();
	    $idClient = JRequest::getVar('idClient');

	    $dataNascita = JRequest::getVar('dataNascita');

	    $professione = JRequest::getVar('professione');

	    $query = $db->getQuery(true);
	    $query->select("idProfessione")->from("#__project_professioni")->where($db->quoteName('idProfessione').' = "'.$professione.'"');
	    $exist = $db->setQuery($query)->loadResult();

	    if(!$exist) {
		    $chiamata = new stdClass();
		    $chiamata->professioneName = $professione;
		    $db->insertObject('#__project_professioni', $chiamata, idProfessione);
		    $professione = $db->insertid();
	    }


	    //dati chiamata
	    $chiamata = new stdClass();
	    $chiamata->nome = JRequest::getVar('nome');
	    $chiamata->cognome = JRequest::getVar('cognome');
	    $chiamata->dataNascita = date("Y-m-d", strtotime(str_replace("/", "-", JRequest::getVar('dataNascita'))));
	    $chiamata->professione = $professione;
	    $chiamata->ral = JRequest::getVar('ral');
	    $chiamata->prevObbli = JRequest::getVar('prevObbliCount');
	    $chiamata->prevLavoro = JRequest::getVar('prevLavoroCount');
	    $chiamata->prevPriv = JRequest::getVar('prevPrivCount');
	    $chiamata->protObbli = JRequest::getVar('protObbliCount');
	    $chiamata->protLav = JRequest::getVar('protLavCount');
	    $chiamata->protPriv = JRequest::getVar('protPrivCount');
	    $chiamata->tutLav = JRequest::getVar('tutLavCount');
	    $chiamata->tutPriv = JRequest::getVar('tutPrivCount');
	    $chiamata->tutCcnl = JRequest::getVar('tutCcnlCount');
	    $chiamata->analisi = JRequest::getVar('analisi', '', 'post', 'string', JREQUEST_ALLOWHTML );
	    $chiamata->conclusioni = JRequest::getVar('conclusioni', '', 'post', 'string', JREQUEST_ALLOWHTML );
	    $chiamata->AnalisiEstrattoConto = JRequest::getVar('AnalisiEstrattoConto', '', 'post', 'string', JREQUEST_ALLOWHTML );
	    $chiamata->luogo = JRequest::getVar('luogo');
	    $chiamata->prevObbliviewPDF = JRequest::getVar('prevObbliviewPDF') ? JRequest::getVar('prevObbliviewPDF') : 0;
	    $chiamata->prevLavoroviewPDF =JRequest::getVar('prevLavoroviewPDF') ? JRequest::getVar('prevLavoroviewPDF') : 0;
	    $chiamata->prevPrivviewPDF = JRequest::getVar('prevPrivviewPDF') ? JRequest::getVar('prevPrivviewPDF') : 0;
	    $chiamata->data = date("Y-m-d", strtotime(str_replace("/", "-", JRequest::getVar('data'))));
	    $chiamata->diagnosiInPDF = JRequest::getVar('diagnosiInPDF');
	    $chiamata->conclusioniInPDF = JRequest::getVar('conclusioniInPDF');
	    $chiamata->analisiInPDF = JRequest::getVar('analisiInPDF');

	    if (JRequest::getVar('data')) {

		    $date1 = new DateTime(date("Y-m-d",strtotime(str_replace("/","-",JRequest::getVar('dataNascita')))));
		    $date2 = $date1->diff(new DateTime(date("Y-m-d",strtotime(str_replace("/","-",JRequest::getVar('data'))))));
		    $chiamata->etaRaggiunta = $date2->y . ' anni ' . $date2->m . ' mesi';

		    if((int)date("m", strtotime(str_replace("/", "-", JRequest::getVar('data')))) <= $date2->m) {

			    $date2->y = $date2->y+1;
		    }
		    $chiamata->etaAnagrafica = $date2->y . ' anni ';
        }
        $chiamata->userId = $user->id;

        if ($idClient != 0) {
            $chiamata->idClient = $idClient;
            $db->updateObject('#__project_clients', $chiamata, idClient);
            $return[0] = 2;
            $return[1] = $idClient;
        } else {
            //ricalcolo nr chiamata per evitare doppioni
            $chiamata->datetime = date('Y-m-d H:i:s');
            $db->insertObject('#__project_clients', $chiamata, idClient);
            $idClient = $db->insertid();
            $return[0] = 1;
            $return[1] = $idClient;
        }

        $prevObbli = jRequest::getVar('prevObbli');


        $query = 'DELETE FROM #__project_prevobbligatorie WHERE idClient = ' . $idClient;
        $db->setQuery($query);
        $db->query();
        $query = 'DELETE FROM #__project_erog_prev_obblig WHERE idClient = ' . $idClient;
        $db->setQuery($query);
        $db->query();
	    var_dump($prevObbli);
        foreach($prevObbli as $key=>$obbli) {

	        $nomeCassaId = $obbli['name'];

	        $query = $db->getQuery(true);
	        $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa').' = "'.$nomeCassaId.'"');
	        $exist = $db->setQuery($query)->loadResult();

	        if(!$exist) {
		        $chiamata = new stdClass();
		        $chiamata->nomeCassa = $nomeCassaId;
		        $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
		        $nomeCassaId = $db->insertid();
	        }

	        //dati chiamata
	        $chiamata = new stdClass();
	        $chiamata->nome = $nomeCassaId;
            $chiamata->primaContr = date("Y-m-d",strtotime(str_replace("/","-",$obbli['prima'])));
            $chiamata->ultimaContr = date("Y-m-d",strtotime(str_replace("/","-",$obbli['ultima'])));
            $chiamata->anzianitaContrAnni = $obbli['anzianitaAnni'];
	        $chiamata->anzianitaContrMesi = $obbli['anzianitaMesi'];
	        $chiamata->anzianitaContrSett = $obbli['anzianitaSett'];
            $chiamata->buchiContr = $obbli['anzianita'];
            $chiamata->riscattabilita = $obbli['anzianita'];
            $chiamata->monteContr = $obbli['monte'];
	        $chiamata->RAL = $obbli['ral'];
            $chiamata->renditaAcquisitaMens = $obbli['renditaAcquisitaMens'];
            $chiamata->renditaAcquisitaAnn = $obbli['renditaAcquisitaAnn'];
            $chiamata->renditaAnnuaAggMens = $obbli['renditaAnnuaAggMens'];
            $chiamata->renditaAnnuaAggAnn = $obbli['renditaAnnuaAggAnn'];
            $chiamata->idClient = $idClient;
            $chiamata->datetime = date('Y-m-d H:i:s');
            $chiamata->userId = $user->id;
            $chiamata->orders = $key;
	        $chiamata->inPDF = $obbli['inPDF'];
            $db->insertObject('#__project_prevobbligatorie', $chiamata, prevObblId);
            $prevObblId = $db->insertid();

            $erogs = $obbli['erogs'];

            foreach($erogs as $key2=>$erog) {

	            $erogTypeId = $erog['erogType'];

	            $query = $db->getQuery(true);
	            $query->select("erogType")->from("#__project_erog_types")->where($db->quoteName('erogType').' = "'.$erogTypeId.'"');
	            $exist = $db->setQuery($query)->loadResult();

	            if(!$exist) {
		            $chiamata = new stdClass();
		            $chiamata->erogName = $erogTypeId;
		            $db->insertObject('#__project_erog_types', $chiamata, erogType);
		            $erogTypeId = $db->insertid();
	            }

                $chiamata = new stdClass();
                $chiamata->erogType = $erogTypeId;
                $chiamata->data = date("Y-m-d",strtotime(str_replace("/","-",$erog['data'])));
                $date1 = new DateTime(date("Y-m-d",strtotime(str_replace("/","-",JRequest::getVar('dataNascita')))));
                $date2 = $date1->diff(new DateTime(date("Y-m-d",strtotime(str_replace("/","-",$erog['data'])))));

                $chiamata->valore = $erog['valore'];
                $chiamata->eta = $date2->y.' anni '.$date2->m.' mesi';
                $chiamata->idClient = $idClient;
                $chiamata->prevObblId = $prevObblId;
                $chiamata->datetime = date('Y-m-d H:i:s');
                $chiamata->userId = $user->id;
                $chiamata->orders = $key2;
	            $chiamata->inPDF = $erog['inPDF'];
	            $chiamata->viewPDF = $erog['viewPDF'];
                $db->insertObject('#__project_erog_prev_obblig', $chiamata, erogObblId);
            }


        }
       // die();
	    $prevLavoro = jRequest::getVar('prevLavoro');

	    $query = 'DELETE FROM #__project_prevlavoro WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    $query = 'DELETE FROM #__project_erog_prev_lavoro WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    $query = 'DELETE FROM #__project_erog_prev_lavoro_rendite WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();

	    foreach($prevLavoro as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa').' = "'.$nomeCassaId.'"');
		    $exist = $db->setQuery($query)->loadResult();

		    if(!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nome = $nomeCassaId;
		    $chiamata->primaContr = date("Y-m-d",strtotime(str_replace("/","-",$obbli['prima'])));
		    $chiamata->ultimaContr = date("Y-m-d",strtotime(str_replace("/","-",$obbli['ultima'])));
		    $chiamata->anzianitaContr = $obbli['anzianita'];
		    $chiamata->monteContr = $obbli['monte'];
		    $chiamata->RAL = $obbli['ral'];
		    $chiamata->renditaAcquisitaMens = $obbli['renditaAcquisitaMens'];
		    $chiamata->renditaAcquisitaAnn = $obbli['renditaAcquisitaAnn'];
		    $chiamata->renditaAnnuaAggMens = $obbli['renditaAnnuaAggMens'];
		    $chiamata->renditaAnnuaAggAnn = $obbli['renditaAnnuaAggAnn'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;
		    $chiamata->inPDF = $obbli['inPDF'];
		    $db->insertObject('#__project_prevlavoro', $chiamata, prevObblId);
		    $prevObblId = $db->insertid();

		    $erogs = $obbli['erogs'];

		    foreach($erogs as $key2=>$erog) {

			    $erogTypeId = $erog['erogType'];

			    $query = $db->getQuery(true);
			    $query->select("erogType")->from("#__project_erog_types")->where($db->quoteName('erogType').' = "'.$erogTypeId.'"');
			    $exist = $db->setQuery($query)->loadResult();

			    if(!$exist) {
				    $chiamata = new stdClass();
				    $chiamata->erogName = $erogTypeId;
				    $db->insertObject('#__project_erog_types', $chiamata, erogType);
				    $erogTypeId = $db->insertid();
			    }

			    $chiamata = new stdClass();
			    $chiamata->erogType = $erogTypeId;
			    $chiamata->data = date("Y-m-d",strtotime(str_replace("/","-",$erog['data'])));
			    $date1 = new DateTime(date("Y-m-d",strtotime(str_replace("/","-",JRequest::getVar('dataNascita')))));
			    $date2 = $date1->diff(new DateTime(date("Y-m-d",strtotime(str_replace("/","-",$erog['data'])))));

			    $chiamata->valore = $erog['valore'];
			    $chiamata->eta = $date2->y.' anni '.$date2->m.' mesi';
			    $chiamata->idClient = $idClient;
			    $chiamata->prevLavId = $prevObblId;
			    $chiamata->datetime = date('Y-m-d H:i:s');
			    $chiamata->userId = $user->id;
			    $chiamata->orders = $key2;
			    $chiamata->inPDF = $erog['inPDF'];
			    $chiamata->viewPDF = $erog['viewPDF'];
			    $db->insertObject('#__project_erog_prev_lavoro', $chiamata, erogObblId);
		    }

		    $rendite = $obbli['rendite'];

		    foreach($rendite as $key2=>$erog) {

			    $nomeCassaId = $erog['renditaType'];
			    if ($nomeCassaId!='') {
				    $query = $db->getQuery(true);
				    $query->select("renditaType")->from("#__project_rendite_types")->where($db->quoteName('renditaType') . ' = "' . $nomeCassaId . '"');
				    $exist = $db->setQuery($query)->loadResult();

				    if (!$exist) {
					    $chiamata = new stdClass();
					    $chiamata->renditaName = $nomeCassaId;
					    $db->insertObject('#__project_rendite_types', $chiamata, renditaType);
					    $nomeCassaId = $db->insertid();
				    }
			    }

			    $modalita = $erog['modalita'];
			    if ($modalita!='') {
				    $query = $db->getQuery(true);
				    $query->select("idModalita")->from("#__project_modalita")->where($db->quoteName('idModalita') . ' = "' . $modalita . '"');
				    $exist = $db->setQuery($query)->loadResult();

				    if (!$exist) {
					    $chiamata = new stdClass();
					    $chiamata->modalitaName = $modalita;
					    $db->insertObject('#__project_modalita', $chiamata, idModalita);
					    $modalita = $db->insertid();
				    }
			    }

			    $chiamata = new stdClass();
			    $chiamata->renditaType = $nomeCassaId;
			    $chiamata->valore = $erog['valore'];
			    $chiamata->modalita = $modalita;
			    $chiamata->idClient = $idClient;
			    $chiamata->prevLavId = $prevObblId;
			    $chiamata->datetime = date('Y-m-d H:i:s');
			    $chiamata->userId = $user->id;
			    $chiamata->orders = $key2;

			    $db->insertObject('#__project_erog_prev_lavoro_rendite', $chiamata, erogObblId);
		    }

	    }
	    $prevPriv = jRequest::getVar('prevPriv');

	    $query = 'DELETE FROM #__project_prevprivata WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    $query = 'DELETE FROM #__project_erog_prev_privata WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();

	    $query = 'DELETE FROM #__project_erog_prev_privata_rendite WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();

	    foreach($prevPriv as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa').' = "'.$nomeCassaId.'"');
		    $exist = $db->setQuery($query)->loadResult();

		    if(!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nome = $nomeCassaId;
		    $chiamata->primaContr = date("Y-m-d",strtotime(str_replace("/","-",$obbli['prima'])));
		    $chiamata->ultimaContr = date("Y-m-d",strtotime(str_replace("/","-",$obbli['ultima'])));
		    $chiamata->anzianitaContr = $obbli['anzianita'];
		    $chiamata->monteContr = $obbli['monte'];
		    $chiamata->RAL = $obbli['ral'];
		    $chiamata->renditaAcquisitaMens = $obbli['renditaAcquisitaMens'];
		    $chiamata->renditaAcquisitaAnn = $obbli['renditaAcquisitaAnn'];
		    $chiamata->renditaAnnuaAggMens = $obbli['renditaAnnuaAggMens'];
		    $chiamata->renditaAnnuaAggAnn = $obbli['renditaAnnuaAggAnn'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;
		    $chiamata->inPDF = $obbli['inPDF'];
		    $db->insertObject('#__project_prevprivata', $chiamata, prevObblId);
		    $prevObblId = $db->insertid();

		    $erogs = $obbli['erogs'];

		    foreach($erogs as $key2=>$erog) {

			    $erogTypeId = $erog['erogType'];

			    $query = $db->getQuery(true);
			    $query->select("erogType")->from("#__project_erog_types")->where($db->quoteName('erogType').' = "'.$erogTypeId.'"');
			    $exist = $db->setQuery($query)->loadResult();

			    if(!$exist) {
				    $chiamata = new stdClass();
				    $chiamata->erogName = $erogTypeId;
				    $db->insertObject('#__project_erog_types', $chiamata, erogType);
				    $erogTypeId = $db->insertid();
			    }

			    $chiamata = new stdClass();
			    $chiamata->erogType = $erogTypeId;
			    $chiamata->data = date("Y-m-d",strtotime(str_replace("/","-",$erog['data'])));
			    $date1 = new DateTime(date("Y-m-d",strtotime(str_replace("/","-",JRequest::getVar('dataNascita')))));
			    $date2 = $date1->diff(new DateTime(date("Y-m-d",strtotime(str_replace("/","-",$erog['data'])))));

			    $chiamata->valore = $erog['valore'];
			    $chiamata->eta = $date2->y.' anni '.$date2->m.' mesi';
			    $chiamata->idClient = $idClient;
			    $chiamata->prevPrivId = $prevObblId;
			    $chiamata->datetime = date('Y-m-d H:i:s');
			    $chiamata->userId = $user->id;
			    $chiamata->orders = $key2;
			    $chiamata->inPDF = $erog['inPDF'];
			    $chiamata->viewPDF = $erog['viewPDF'];
			    $db->insertObject('#__project_erog_prev_privata', $chiamata, erogObblId);
		    }

		    $rendite = $obbli['rendite'];

		    foreach($rendite as $key2=>$erog) {

			    $nomeCassaId = $erog['renditaType'];
			    if ($nomeCassaId!='') {
				    $query = $db->getQuery(true);
				    $query->select("renditaType")->from("#__project_rendite_types")->where($db->quoteName('renditaType') . ' = "' . $nomeCassaId . '"');
				    $exist = $db->setQuery($query)->loadResult();

				    if (!$exist) {
					    $chiamata = new stdClass();
					    $chiamata->renditaName = $nomeCassaId;
					    $db->insertObject('#__project_rendite_types', $chiamata, renditaType);
					    $nomeCassaId = $db->insertid();
				    }
			    }

			    $modalita = $erog['modalita'];
			    if ($modalita!='') {
				    $query = $db->getQuery(true);
				    $query->select("idModalita")->from("#__project_modalita")->where($db->quoteName('idModalita') . ' = "' . $modalita . '"');
				    $exist = $db->setQuery($query)->loadResult();

				    if (!$exist) {
					    $chiamata = new stdClass();
					    $chiamata->modalitaName = $modalita;
					    $db->insertObject('#__project_modalita', $chiamata, idModalita);
					    $modalita = $db->insertid();
				    }
			    }

			    $chiamata = new stdClass();
			    $chiamata->renditaType = $nomeCassaId;
			    $chiamata->valore = $erog['valore'];
			    $chiamata->modalita = $modalita;
			    $chiamata->idClient = $idClient;
			    $chiamata->prevPrivId = $prevObblId;
			    $chiamata->datetime = date('Y-m-d H:i:s');
			    $chiamata->userId = $user->id;
			    $chiamata->orders = $key2;
			    $db->insertObject('#__project_erog_prev_privata_rendite', $chiamata, erogObblId);
		    }

	    }
	    $protObbli = JRequest::getVar('protObbli');
	    $query = 'DELETE FROM #__project_protobbligatorie WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    foreach($protObbli as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa') . ' = "' . $nomeCassaId . '"');
		    $exist = $db->setQuery($query)->loadResult();

		    if (!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nome = $nomeCassaId;
		    $chiamata->requisitoRaggiunto = $obbli['requisitoRaggiunto'];
		    $chiamata->morteMens = $obbli['morteMens'];
		    $chiamata->morteAnn = $obbli['morteAnn'];
		    $chiamata->inabilitaMens = $obbli['inabilitaMens'];
		    $chiamata->inabilitaAnn = $obbli['inabilitaAnn'];
		    $chiamata->invaliditaMens = $obbli['invaliditaMens'];
		    $chiamata->invaliditaAnn = $obbli['invaliditaAnn'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;
		    $chiamata->inPDF = $obbli['inPDF'];
		    $db->insertObject('#__project_protobbligatorie', $chiamata, protObblId);
		    $prevObblId = $db->insertid();
	    }

	    $protLav = JRequest::getVar('protLav');
	    $query = 'DELETE FROM #__project_protlavoro WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    foreach($protLav as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa') . ' = "' . $nomeCassaId . '"');
		    $exist = $db->setQuery($query)->loadResult();

		    if (!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nome = $nomeCassaId;
		    $chiamata->invaliditaInfortunio = $obbli['invaliditaInfortunio'];
		    $chiamata->invaliditaMalattia = $obbli['invaliditaMalattia'];
		    $chiamata->morte = $obbli['morte'];
		    $chiamata->LTC = $obbli['LTC'];
		    $chiamata->LTCVal = $obbli['LTCVal'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;
		    $chiamata->inPDF = $obbli['inPDF'];
		    $db->insertObject('#__project_protlavoro', $chiamata, protLavId);
		    $prevObblId = $db->insertid();
	    }

	    $protPriv = JRequest::getVar('protPriv');
	    $query = 'DELETE FROM #__project_protprivata WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    foreach($protPriv as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa') . ' = "' . $nomeCassaId . '"');
		    $exist = $db->setQuery($query)->loadResult();

		    if (!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nome = $nomeCassaId;
		    $chiamata->invaliditaInfortunio = $obbli['invaliditaInfortunio'];
		    $chiamata->invaliditaMalattia = $obbli['invaliditaMalattia'];
		    $chiamata->morte = $obbli['morte'];
		    $chiamata->LTC = $obbli['LTC'];
		    $chiamata->LTCVal = $obbli['LTCVal'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;
		    $chiamata->inPDF = $obbli['inPDF'];
		    $db->insertObject('#__project_protprivata', $chiamata, protPrivId);
		    $prevObblId = $db->insertid();
	    }


	    $tutLav = JRequest::getVar('tutLav');
	    $query = 'DELETE FROM #__project_tutlavoro WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    foreach($tutLav as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa') . ' = "' . $nomeCassaId . '"');
		    $exist = $db->setQuery($query)->loadResult();

		    if (!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nomeTut = $nomeCassaId;
		    $chiamata->costoAnn = $obbli['costoAnn'];
		    $chiamata->prevenzione = $obbli['prevenzione'];
		    $chiamata->diagnosi = $obbli['diagnosi'];
		    $chiamata->interventi = $obbli['interventi'];
		    $chiamata->riabilitazione = $obbli['riabilitazione'];
		    $chiamata->LTC = $obbli['LTC'];
		    $chiamata->lentiDenti = $obbli['lentiDenti'];
		    $chiamata->assistenza = $obbli['assistenza'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;

		    $db->insertObject('#__project_tutlavoro', $chiamata, protPrivId);
		    $prevObblId = $db->insertid();
	    }

	    $tutPriv = JRequest::getVar('tutPriv');
	    $query = 'DELETE FROM #__project_tutprivata WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    foreach($tutPriv as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa') . ' = "' . $nomeCassaId . '"');
		    $exist = $db->setQuery($query)->loadResult();

		    if (!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nomeTut = $nomeCassaId;
		    $chiamata->costoAnn = $obbli['costoAnn'];
		    $chiamata->prevenzione = $obbli['prevenzione'];
		    $chiamata->diagnosi = $obbli['diagnosi'];
		    $chiamata->interventi = $obbli['interventi'];
		    $chiamata->riabilitazione = $obbli['riabilitazione'];
		    $chiamata->LTC = $obbli['LTC'];
		    $chiamata->lentiDenti = $obbli['lentiDenti'];
		    $chiamata->assistenza = $obbli['assistenza'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;

		    $db->insertObject('#__project_tutprivata', $chiamata, protPrivId);
		    $prevObblId = $db->insertid();
	    }

	    $tutCcnl = JRequest::getVar('tutCcnl');
	    $query = 'DELETE FROM #__project_tutccnl WHERE idClient = ' . $idClient;
	    $db->setQuery($query);
	    $db->query();
	    foreach($tutCcnl as $key=>$obbli) {

		    $nomeCassaId = $obbli['name'];

		    $query = $db->getQuery(true);
		    $query->select("idCassa")->from("#__project_nomi_casse")->where($db->quoteName('idCassa') . ' = "' . $nomeCassaId . '"');
		    $exist = $db->setQuery($query)->loadResult();

		    if (!$exist) {
			    $chiamata = new stdClass();
			    $chiamata->nomeCassa = $nomeCassaId;
			    $db->insertObject('#__project_nomi_casse', $chiamata, nomeCassaId);
			    $nomeCassaId = $db->insertid();
		    }

		    //dati chiamata
		    $chiamata = new stdClass();
		    $chiamata->nomeTut = $nomeCassaId;
		    $chiamata->costoAnn = $obbli['costoAnn'];
		    $chiamata->prevenzione = $obbli['prevenzione'];
		    $chiamata->diagnosi = $obbli['diagnosi'];
		    $chiamata->interventi = $obbli['interventi'];
		    $chiamata->riabilitazione = $obbli['riabilitazione'];
		    $chiamata->LTC = $obbli['LTC'];
		    $chiamata->lentiDenti = $obbli['lentiDenti'];
		    $chiamata->assistenza = $obbli['assistenza'];
		    $chiamata->idClient = $idClient;
		    $chiamata->datetime = date('Y-m-d H:i:s');
		    $chiamata->userId = $user->id;
		    $chiamata->orders = $key;

		    $db->insertObject('#__project_tutccnl', $chiamata, protPrivId);
		    $prevObblId = $db->insertid();
	    }
        return $return;
    }

	public function delete() {
		$db = JFactory::getDBO();

		$idClient = JRequest::getVar('cid');

		$chiamata = new stdClass();
		$chiamata->deleted = 1;
		$chiamata->idClient = $idClient;
		$db->updateObject('#__project_clients', $chiamata, idClient);

		return;
	}

	public function getErogstypes() {
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select("*")->from("#__project_erog_types");

		return $db->setQuery($query)->loadObjectList();
	}
	public function getCasse() {
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select("*")->from("#__project_nomi_casse");

		return $db->setQuery($query)->loadObjectList();
	}

	public function getRenditetypes() {
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select("*")->from("#__project_rendite_types");

		return $db->setQuery($query)->loadObjectList();
	}

	public function getModalita() {
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select("*")->from("#__project_modalita");

		return $db->setQuery($query)->loadObjectList();
	}

	public function getProfessioni() {
		$db = JFactory::getDBO();

		$query = $db->getQuery(true);
		$query->select("*")->from("#__project_professioni");

		return $db->setQuery($query)->loadObjectList();
	}

    public function deleteChiamata() {
        $db = JFactory::getDBO();
        $idChiamata = JRequest::getVar('idChiamata');

        //cancello chiamata
        $query = 'DELETE FROM #__osma_chiamate WHERE idChiamata = ' . $idChiamata;
        $db->setQuery($query);
        $db->query();

        //cancello interventi
        $query = 'DELETE FROM #__osma_chiamata_interventi WHERE idChiamata = ' . $idChiamata;
        $db->setQuery($query);
        $db->query();

        return true;
    }

    public function getComunicazioni() {
        $idChiamata = JRequest::getVar('idChiamata');
        if (!$idChiamata) {
            return;
        }
        $db = JFactory::getDBO();

        $query = $db->getQuery(true);
        $query->select("*")->from("#__osma_chiamate_comunicazioni")->where("idChiamata = {$idChiamata}");

        return $db->setQuery($query)->loadObjectList();
    }

    public function generaPdf() {

	    $idClient = JRequest::getVar('idClient');

	    $db = JFactory::getDbo();
	    $query = $db->getQuery(true);
	    $query->select('*')->from('#__project_clients as a')
		    ->join('left','#__project_professioni as b ON a.professione = b.idProfessione')
		    ->where('a.idClient='.$idClient);
	    $db->setQuery($query);
	    $client = $db->loadObject();

	    //recupero le previdenze con tutte le casse
	    //obbligatoria
	    $query = $db->getQuery(true);
	    $query->select('*')->from('#__project_prevobbligatorie as a')
		    ->join('left','#__project_nomi_casse as b ON a.nome = b.idCassa')
		    ->where('a.idClient='.$idClient);
	    $db->setQuery($query);
	    $client->prev->obblig = $db->loadObjectList();
	    $num = count($client->prev->obblig);
	    if($num>0 || $client->prevObbliviewPDF==1) {
		    $sum = 0;
		    foreach ($client->prev->obblig as $obblig) {
			    $sum = $obblig->monteContr + $sum;
			    $obbligValuesChart[$obblig->nomeCassa] = $obblig->monteContr;
		    }
		    $totalObblig = $sum;
		    arsort($obbligValuesChart);
		    $obbligValuesChart = urlencode(serialize($obbligValuesChart));
		    $totalPrev['Obbligatoria'] = $totalObblig;

		    $previdenze['Previdenza Obbligatoria'] = $obbligValuesChart;
	    } else {
		    $totalObblig = 0;
		    $obbligValuesChart = "";
	    }
	    //var_dump($num);die();

	    //lavoro
	    $query = $db->getQuery(true);
	    $query->select('*')->from('#__project_prevlavoro as a')
		    ->join('left','#__project_nomi_casse as b ON a.nome = b.idCassa')
		    ->where('a.idClient='.$idClient);
	    $db->setQuery($query);
	    $client->prev->lavoro = $db->loadObjectList();
	    $num = count($client->prev->lavoro);
	    if($num>0 || $client->prevLavoroviewPDF==1) {
		    $sum = 0;
		    foreach ($client->prev->lavoro as $obblig) {
			    $sum = $obblig->monteContr  + $sum;
			    $lavoroValuesChart[$obblig->nomeCassa] = $obblig->monteContr;
		    }
		    $totalLavoro = $sum;
		    arsort($lavoroValuesChart);
		    $lavoroValuesChart = urlencode(serialize($lavoroValuesChart));
		    $totalPrev['Aziendale'] = $totalLavoro;

		    $previdenze['Previdenza Aziendale'] = $lavoroValuesChart;
	    } else {
		    $totalLavoro = 0;
		    $lavoroValuesChart = "";

	    }


	    //privata
	    $query = $db->getQuery(true);
	    $query->select('*')->from('#__project_prevprivata as a')
		    ->join('right','#__project_nomi_casse as b ON a.nome = b.idCassa')
		    ->where('a.idClient='.$idClient);
	    $db->setQuery($query);
	    $client->prev->priv = $db->loadObjectList();

	    $num = count($client->prev->priv);

	    if($num>0 || $client->prevPrivviewPDF==1) {
		    $sum = 0;
		    foreach ($client->prev->priv as $obblig) {
			    $sum = $obblig->monteContr  + $sum;
			    $privValuesChart[$obblig->nomeCassa] = $obblig->monteContr;
		    }
		    $totalPriv = $sum;
		    arsort($privValuesChart);
		    $privValuesChart = urlencode(serialize($privValuesChart));
		    $totalPrev['Privata'] = $totalPriv;

		    $previdenze['Previdenza Privata'] = $privValuesChart;
	    } else {
		    $totalPriv = 0;
		    $privValuesChart = "";

	    }


	    $totalPrev = urlencode(serialize($totalPrev));



	    $query = $db->getQuery(true);
	    $query->select('*,a.inPDF as showinpdf')->from('#__project_erog_prev_obblig as a')
		    ->join('left','#__project_erog_types as b ON a.erogType = b.erogType')
		    ->join('left','#__project_prevobbligatorie as c ON a.prevObblId = c.prevObblId')
		    ->join('left','#__project_nomi_casse as d ON c.nome = d.idCassa')
		    ->where('a.idClient='.$idClient)
		    //->where('a.inPDF=1')
		    ->order('a.orders ASC');
	    $db->setQuery($query);
	    $client->erogs->obblig = $db->loadObjectList();
	    //var_dump($client->erogs->obblig);die();
		$sum = 0;
	    foreach ($client->erogs->obblig as $obblig) {
	    	$erogObbli[$obblig->prevObblId][] = $obblig;

	    	$sum = $obblig->valore  + $sum;
	    	if($obblig->viewPDF==2) {
			    $obblig->valore = $obblig->valore*13;
		    }
		    $obbligvalues[$obblig->erogName] = $obblig->valore;

	    }

	    $query = $db->getQuery(true);
	    $query->select('*,a.inPDF as showinpdf')->from('#__project_erog_prev_lavoro as a')
		    ->join('left','#__project_erog_types as b ON a.erogType = b.erogType')
		    ->join('left','#__project_prevlavoro as c ON a.prevLavId = c.prevLavId')
		    ->join('left','#__project_nomi_casse as d ON c.nome = d.idCassa')
		    ->where('a.idClient='.$idClient)
		    //->where('a.inPDF=1')
		    ->order('a.orders ASC');
	    $db->setQuery($query);
	    $client->erogs->lavoro = $db->loadObjectList();
	    //var_dump($client->erogs->obblig);die();
	    $sum = 0;
	    foreach ($client->erogs->lavoro as $obblig) {
		    $erogLav[$obblig->prevLavId][] = $obblig;
		    $sum = $obblig->valore  + $sum;
		    if($obblig->viewPDF==2) {
			    $obblig->valore = $obblig->valore*12;
		    }
		    $lavvalues[$obblig->erogName] = $obblig->valore;
	    }

	    $query = $db->getQuery(true);
	    $query->select('*,a.inPDF as showinpdf')->from('#__project_erog_prev_privata as a')
		    ->join('left','#__project_erog_types as b ON a.erogType = b.erogType')
		    ->join('left','#__project_prevprivata as c ON a.prevPrivId = c.prevPrivId')
		    ->join('left','#__project_nomi_casse as d ON c.nome = d.idCassa')
		    ->where('a.idClient='.$idClient)
		    //->where('a.inPDF=1')
		    ->order('a.orders ASC');
	    $db->setQuery($query);
	    $client->erogs->priv = $db->loadObjectList();
	    //var_dump($client->erogs->obblig);die();
	    $sum = 0;
	    foreach ($client->erogs->priv as $obblig) {
		    $erogPriv[$obblig->prevPrivId][] = $obblig;
		    $sum = $obblig->valore  + $sum;
		    if($obblig->viewPDF==2) {
			    $obblig->valore = $obblig->valore*12;
		    }
		    $privvalues[$obblig->erogName] = $obblig->valore;
	    }




	    $mainColor = '#52b152';



	    ob_start();
	    include('components/com_project4life/templates/pdf.php');
	    $html = ob_get_contents();
	    ob_end_clean();
	    if(JRequest::getVar('debug')) {
		    var_dump($html);
		    die();
	    }

        require_once(dirname(__FILE__) . '/../lib/libpdf/tcpdf_include.php');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetKeywords('');

	    $pdf->SetFont('montserratlight', '', 11, '', false);
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', PDF_HEADER_STRING);
	    $pdf->setHeaderData('',0,'','',array(0,0,0), array(255,255,255) );
	    $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
	    $pdf->SetMargins(10, 5, 10, true); // set the margins
        //$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        if (@file_exists(dirname(__FILE__) . '/../libpdf/lang/ita.php')) {
            require_once(dirname(__FILE__) . '/../libpdf/lang/ita.php');
            $pdf->setLanguageArray($l);
        }

        $pdf->AddPage();




        $pdf->writeHTML($html, true, false, true, false, '');
        $pdf->lastPage();
        $pdf->Output('tmp/pratica_'.$client->idClient.'.pdf', 'F'); //sostituire I con F per creare il file
        //stampa
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: public");
        header("Content-Description: File Transfer");
        header("Content-type: application/pdf");
        header('Content-Disposition: attachment; filename="pratica_'.$client->idClient.'.pdf"');
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: " . filesize('tmp/pratica_'.$client->idClient.'.pdf'));
        ob_end_flush();
        readfile('tmp/pratica_'.$client->idClient.'.pdf');
        // remove zip file from temp path
        unlink('tmp/pratica_'.$client->idClient.'.pdf');
    }

    function upload() {
        jimport('joomla.filesystem.file');
        $file = JRequest::getVar('file', null, 'files', 'array');
        $type = JRequest::getVar('type');
        if ($file['tmp_name']) {
            //Import filesystem libraries. Perhaps not necessary, but does not hurt
            jimport('joomla.filesystem.file');
            //Clean up filename to get rid of strange characters like spaces etc
            //Set up the source and destination of the file
            $src = $file['tmp_name'];
            $orig = JPATH_ROOT . DS . 'components' . DS . 'com_osma' . DS . "files" . DS . "chiamate" . DS . "1" . DS . $src;

            //First check if the file has the right extension, we need jpg only

            if (JFile::upload($src, $orig)) {//upload immagine originale
            } else {
                //Redirect and throw an error message
            }
        }
    }

}
