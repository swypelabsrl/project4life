<?php
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
$userid = $user->get('id');
?>

<div class="t3a-toolbar row">
	<!--
    <div class="col-sm-12 hidden-xs">
        <?php /*echo $this->loadTemplate('form'); */?>
    </div>
    <div class="visible-xs col-xs-12 text-center"style="padding-top: 15px; padding-bottom: 15px">
        <a href="#filter-collapse" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="filter-collapse"><i class="glyphicon glyphicon-filter"></i> Azioni e filtri</a>
    </div>
    <div class="col-xs-12 collapse" id="filter-collapse">
        <?php /*echo $this->loadTemplate('form'); */?>
    </div>
    -->
	<a href="index.php?option=com_project4life&view=client" class="btn btn-success">Aggiungi</a>
</div>

<form action="<?php echo JRoute::_('index.php?option=com_project4life&view=clients'); ?>" method="post" id="adminForm" class="form-validate clientsForm" name="clientsForm">

    <div class="panel panel-default main-panel" id="chiamate-print">
        <div class="panel-heading">
            <h3 class="panel-title">Elenco Pratiche (<?php echo $this->get("Total") ?>)</h3>
        </div>
        <div class="panel-body">

            <div class="row pagination-top t3a-pagination">
                <div class="col-xs-12 col-sm-8 text-left"><?php echo $this->pagination->getListFooter(); ?></div>
                <div class="col-xs-12 col-sm-4 text-right">Nr. Risultati:
                    <select id="limit" name="limit" class="inputbox input-mini" size="1" onchange="this.form.submit()">
                        <option value="5" <?php echo $this->limit == 5 ? 'selected="selected"' : null ?>>5</option>
                        <option value="10" <?php echo $this->limit == 10 ? 'selected="selected"' : null ?>>10</option>
                        <option value="15" <?php echo $this->limit == 15 ? 'selected="selected"' : null ?>>15</option>
                        <option value="20" <?php echo $this->limit == 20 ? 'selected="selected"' : null ?>>20</option>
                        <option value="25" <?php echo $this->limit == 25 ? 'selected="selected"' : null ?>>25</option>
                        <option value="30" <?php echo $this->limit == 30 ? 'selected="selected"' : null ?>>30</option>
                        <option value="50" <?php echo $this->limit == 50 ? 'selected="selected"' : null ?>>50</option>
                    </select>
                </div>
            </div>

            <?php if (!empty($this->clients)) { ?>
                <?php $i = 0; ?>
                <?php foreach ($this->clients as $i => $chiamata) { ?>
                    <div class='clearfix panel-wrapper flex flex-wrap'>
                        <div class='col-lg-11 col-sm-12 col-xs-12 to-print'>
                            <div class="row panel panel-default panelCompagnia"  style="border-color: <?php echo $chiamata->compagniaColor; ?>!important;">
                                <div class="panel-body">
                                    <div class="row intestazione">
                                        <div class="hidden-print visible-sm visible-xs col-xs-12 col-sm-12 text-right">
                                            <h2><?php echo $chiamata->idClient; ?> <i class="fa fa-circle status-<?php echo (JFilterOutput::stringURLSafe($statoChiamata[$chiamata->stato])) ?>"></i></h2>
                                        </div>
                                       <!-- <div class="compagnia hidden-xs col-sm-3 col-md-2 <?php /*echo $cCompagnia; */?>">
                                            <div class="imgContainer text-center">
                                                <?php /*if (JFile::exists(JPATH_SITE . DS . 'components' . DS . 'com_osma' . DS . 'files' . DS . 'compagnie' . DS . $chiamata->idCompagnia . '.jpg')) { */?>
                                                    <img class="img-responsive cImage" src="<?php /*echo JURI::root(true) . '/components/com_osma/files/compagnie/' . $chiamata->idCompagnia . '.jpg'; */?>" />
                                                <?php /*} else { */?>
                                                    <img class="img-responsive cImage" src="<?php /*echo JURI::root(true) . '/components/com_osma/files/generic/no_img.gif'; */?>" />
                                                <?php /*} */?>
                                            </div>
                                        </div>-->
                                        <div class="col-xs-12 col-sm-9 col-md-5">
                                            <label>Cliente:</label>
											<?php echo $chiamata->nome. " ". $chiamata->cognome; ?>
										</div>
                                        <!--<div class="hidden-xs hidden-sm col-md-5 text-right">
                                            <h2><?php /*echo $chiamata->nrChiamata; */?> <i class="fa fa-circle hidden-print status-point status-<?php /*echo (JFilterOutput::stringURLSafe($statoChiamata[$chiamata->stato])) */?>"></i></h2>
                                        </div>-->
                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class='col-lg-1 visible-lg hide-print side-panel'>
                            <div class='row'>
                                <div class="btn-group btn-group-vertical btn-group-square">
                                        <a class="btn" style="min-height:auto;" href="<?php echo JRoute::_('index.php?option='.COMNAME.'&view=client&idClient=' . $chiamata->idClient); ?>">
                                            Visualizza
                                        </a>
                                </div>
                            </div>
                        </div>


                    </div>
                <?php } ?>
            <?php } else { ?>
                <p>Nessuna chiamata inserita.</p>
            <?php } ?>
        </div>
    </div>
    <input type="hidden" name="task" value=""/>
    <input type="hidden" name="boxchecked" value="0"/>
    <?php echo JHtml::_('form.token'); ?>
    <input type="hidden" id="ids" name="ids" />
</form>
