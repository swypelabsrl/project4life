<?php
defined('_JEXEC') or die('Restricted access');
?>
<div class="row clearfix text-right">
    <div class="btn-group btn-group-small col-xs-12 col-sm-offset-6 col-sm-6">
        
    </div>
</div>
<form action="<?php echo JRoute::_('index.php?option=com_project4life&view=clients'); ?>" method="POST" id="search" class="form-validate search t3a-form" id="t3a-form" name="search">
    <div class="flex flex-wrap row">
        <div class="field col-sm-3 col-xs-12">
            <div class='control-group has-icon'>
                <input type="text" placeholder="Ricerca" name="filter_search" id="filter_search" value="<?php echo $this->searchterms; ?>" class="has-icon-left"/>
                <span class="control-group-icon fa fa-search" data-toggle="filter_data"></span>
            </div>
        </div>
        <div class="field col-sm-3 col-xs-6">
            <select name="filter_status" id="filter_status" class='chosen' data-placeholder="Stato">
                <option value="" <?php echo(!$this->status ? "selected='selected'" : "") ?>></option>
                <option value="1" <?php echo($this->status == 1 ? "selected='selected'" : "") ?>>Aperte</option>
                <option value="2" <?php echo($this->status == 2 ? "selected='selected'" : "") ?>>In corso</option>
                <option value="3" <?php echo($this->status == 3 ? "selected='selected'" : "") ?>>Sospese</option>
                <option value="7" <?php echo($this->status == 7 ? "selected='selected'" : "") ?>>Da preventivare</option>
                <option value="5" <?php echo($this->status == 5 ? "selected='selected'" : "") ?>>In attesa di autorizzazione intervento</option>
                <option value="6" <?php echo($this->status == 6 ? "selected='selected'" : "") ?>>In attesa di autorizzazione fatturazione</option>
                <option value="4" <?php echo($this->status == 4 ? "selected='selected'" : "") ?>>Chiuse</option>
            </select>
        </div>
        <div class="field col-sm-3 col-xs-6">
            <select name="filter_type" id="filter_type" class='chosen' data-placeholder="Tipo di intervento">
                <option value <?php echo(!$this->type ? "selected='selected'" : "") ?>></option>
                <option value="1" <?php echo($this->type == 1 ? "selected='selected'" : "") ?>>Meccanico</option>
                <option value="2" <?php echo($this->type == 2 ? "selected='selected'" : "") ?>>Edile</option>
                <option value="3" <?php echo($this->type == 3 ? "selected='selected'" : "") ?>>Elettrico</option>
                <option value="4" <?php echo($this->type == 4 ? "selected='selected'" : "") ?>>Elettronico</option>
                <option value="5" <?php echo($this->type == 5 ? "selected='selected'" : "") ?>>Video Sorveglianza</option>
                <option value="6" <?php echo($this->type == 6 ? "selected='selected'" : "") ?>>Antintrusione</option>
            </select>
        </div>
        <div class="field col-sm-3 col-xs-6">
            <select name="filter_chiamata" id="filter_chiamata" class='chosen' data-placeholder="Tipo di chiamata">
                <option value <?php echo(!$this->tipoChiamataFilter ? "selected='selected'" : "") ?>></option>
                <option value="1" <?php echo($this->tipoChiamataFilter == 1 ? "selected='selected'" : "") ?>>Forfait</option>
                <option value="2" <?php echo($this->tipoChiamataFilter == 2 ? "selected='selected'" : "") ?>>Extra Forfait</option>
                <option value="3" <?php echo($this->tipoChiamataFilter == 3 ? "selected='selected'" : "") ?>>Programmata</option>
                <option value="4" <?php echo($this->tipoChiamataFilter == 4 ? "selected='selected'" : "") ?>>Giroluci</option>
                <option value="5" <?php echo($this->tipoChiamataFilter == 5 ? "selected='selected'" : "") ?>>Presa Visione</option>
            </select>
        </div>

        <div class="field margin-top-sm col-sm-3 col-xs-6">
            <select name="filter_comune" id="filter_comune" class="chosen" data-placeholder="Seleziona città">
                <option value="" <?php echo(!$this->comuneFilter ? "selected='selected'" : "") ?>></option>
                <?php foreach ($this->comuni as $comune) { ?>
                    <option value="<?php echo $comune->comune; ?>" <?php echo ($this->comuneFilter == $comune->comune ? "selected='selected'" : "") ?>><?php echo $comune->comune; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="field margin-top-sm col-sm-3 col-xs-6">
            <div class='control-group date has-icon'>
                <input name="filter_data" id="filter_data" type='text' class="has-icon-left" placeholder='Data intervento' value="<?php echo $this->dataFilter; ?>"/>
                <span class="control-group-icon fa fa-calendar pointer" data-toggle="filter_data"></span>
            </div>
        </div>
        <div class="field margin-top-sm col-sm-3 col-xs-12 text-center">
            <div class="btn-group btn-group-small">
                <span class="col-sm-2 hidden-xs"></span>
                <input type="submit" class="btn col-sm-4 col-xs-6" value="<?php echo JText::_('Trova'); ?>" />
                <button class="btn reset-form col-sm-4 col-xs-6"><?php echo JText::_('Reset'); ?></button>
            </div>
        </div>
    </div>
    <input type="hidden" name="filter_scadute" value="<?php echo $this->scaduteFilter; ?>" />
    <input type="hidden" name="filter_daconsuntivare" value="<?php echo $this->daconsuntivareFilter; ?>" />
</form> 