<?php

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

class ProjectViewClient extends JViewLegacy {

    function display($tpl = null) {
        // Assign data to the view
        $this->title = "Gestione Pratica";
        $this->form = $this->get('Form');
        $this->client = $this->get('Item');
	    $this->erogsTypes = $this->get('Erogstypes');
	    $this->modalita = $this->get('Modalita');
	    $this->professioni = $this->get('Professioni');
	    $this->renditeTypes = $this->get('Renditetypes');
	    $this->casse = $this->get('Casse');


        JFactory::getDocument()->addScript(JURI::base() . 'components/com_project4life/assets/js/view-client.js', 'text/javascript');

        // Display the view
        parent::display($tpl);
    }

}
