<?php
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
$userid = $user->get('id');


?>
<div class="t3a-toolbar row">
    <div class="btn-group btn-group-small pull-right hidden-xs">
        <a href="<?php echo JRoute::_('index.php?option=' . COMNAME . '&view=clients'); ?>" class="btn btn-warning">
            <i class="fa fa-chevron-left"></i> <?php echo "Indietro"; ?>
        </a>
		<?php if(JRequest::getVar('idClient')) { ?>
			<button class="btn btn-danger elimina"  onclick="var result = confirm('Vuoi eliminare l\'intera pratica?');if (result) {location.href='index.php?option=com_project4life&task=client.delete&cid=<?php echo JRequest::getVar('idClient')?>'};"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo "Elimina"; ?>
			</button>
		<a href="<?php echo JRoute::_('index.php?option=' . COMNAME . '&task=client.generaPDF&idClient='.JRequest::getVar('idClient')); ?>" class="btn btn-primary">
			<i class="fa fa-file"></i> <?php echo "Genera PDF"; ?>
		</a>
		<?php } ?>

        <button class="btn btn-primary submitChiamataForm"><i class="fa fa-floppy-o"></i> <?php echo "Salva"; ?>
        </button>
    </div>
    <div class="btn-group btn-group-small pull-right col-xs-12 visible-xs">
        <a href="<?php echo JRoute::_('index.php?option=' . COMNAME . '&view=clients'); ?>" class="btn btn-warning col-xs-3">
            <i class="fa fa-chevron-left"></i> <?php echo "Indietro"; ?>
        </a>
        <button class="btn btn-danger elimina col-xs-3"><i class="fa fa-trash" aria-hidden="true"></i> <?php echo "Elimina"; ?></button>
        <button class="btn btn-primary submitChiamataForm col-xs-3"><i class="fa fa-floppy-o"></i> <?php echo "Salva"; ?></button>
    </div>
</div>
<div class="panel panel-default main-panel">
    <div class="panel-heading" style="display:flex;align-items:center;">
        <h3 class="panel-title">Nuova Pratica</h3>
    </div>

    <div class="panel-body">
        <div class="row">
            <form method="post"  action="<?php echo JRoute::_('index.php?option=' . COMNAME . '&view=client&task=client.salva'); ?>"  id="clientForm" name="clientForm">
                <fieldset class='t3a-form'>
                    <div class='field col-sm-4 col-md-4 col-xs-6'>
                        <label>pratica nr.</label>
                        <input type="text" name="idClient" id="idClient" class="form-control" readonly="readonly" value="<?php echo $this->client->idClient; ?>">
                    </div>
                    <div class='field col-sm-4 col-md-4 col-xs-6'>
                        <label>Luogo</label>
                        <input type="text" name="luogo" id="luogo" class="form-control" value="<?php echo $this->client->luogo; ?>">
                    </div>
                    <div class='field col-sm-4 col-md-4 col-xs-6'>
                        <label>Data</label>
                        <div class='input-group date1' id="date1">
                            <input type='text' class="form-control" id='data' name="data" value="<?php echo(strtotime($this->client->data) ? date("d/m/Y H:i", strtotime($this->client->data)) : date("d/m/Y H:i")) ?>"/>
                            <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class='field col-sm-4 col-md-4 col-xs-6'>
                        <label>Nome</label>
                        <input type="text" name="nome" id="nome" class="form-control" value="<?php echo $this->client->nome; ?>">
                    </div>
                    <div class='field col-sm-4 col-md-4 col-xs-6'>
                        <label>Cognome</label>
                        <input type="text" name="cognome" id="cognome" class="form-control"  value="<?php echo $this->client->cognome; ?>">
                    </div>
                    <div class='field col-sm-4 col-md-4 col-xs-6'>
                        <div class="row">
                            <div class='field col-sm-4 col-md-4 col-xs-6'>
                                <label>Data di Nascita</label>
                                <div class='input-group date2' id="date2">
                                    <input type='text' class="form-control" id='dataNascita' name="dataNascita" value="<?php echo(strtotime($this->client->dataNascita) ? date("d/m/Y H:i", strtotime($this->client->dataNascita)) : date("d/m/Y H:i")) ?>"/>
                                    <span class="input-group-addon">
                                <span class="glyphicon glyphicon-calendar"></span>
                            </span>
                                </div>
                            </div>
                            <div class='field col-sm-4 col-md-4 col-xs-6'>
                                <label>Età raggiunta</label>
                                <input type="text" name="etaRaggiunta" id="etaRaggiunta" class="form-control" value="<?php echo $this->client->etaRaggiunta; ?>">
                            </div>
                            <div class='field col-sm-4 col-md-4 col-xs-6'>
                                <label>Età Prossima</label>
                                <input type="text" name="etaAnagrafica" id="etaAnagrafica" class="form-control" value="<?php echo $this->client->etaAnagrafica; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class='field col-sm-4 col-md-6 col-xs-6'>
                        <label>Professione</label>
						<select name="professione" id="professione" class="form-control actChosen">
							<option value=""></option>
		                    <?php foreach ($this->professioni as $professione) {
			                    $selected = ($professione->idProfessione==$this->client->professione) ? "selected='selected'": ""; ?>
								<option value="<?php echo $professione->idProfessione; ?>" <?php echo $selected; ?>><?php echo $professione->professioneName; ?></option>
		                    <?php } ?>
						</select>
                    </div>
                    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
                        <label>RAL anno precedente</label>
                        <input type="text" name="ral" id="ral" class="form-control " value="<?php echo $this->client->ral; ?>">
                    </div>
					<div class="clearfix"></div>


					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Previdenza</a></li>
						<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Protezione</a></li>
						<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Tutele Sanitarie</a></li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. previdenze obbligatorie</label>
								<input type="number" name="prevObbliCount" id="prevObbli" class="form-control" value="<?php echo $this->client->prevObbli; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Vis. PDF se vuota</label>
								<input type="checkbox" class="form-control" id="prevObbliviewPDF" name="prevObbliviewPDF" <?php echo ($this->client->prevObbliviewPDF==1) ? "checked='checked'" : "";?> value="1"/>
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. previdenze lavoro</label>
								<input type="number" name="prevLavoroCount" id="prevLavoro" class="form-control" value="<?php echo $this->client->prevLavoro; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Vis. PDF se vuota</label>
								<input type="checkbox" class="form-control" id="prevLavoroviewPDF" name="prevLavoroviewPDF" <?php echo ($this->client->prevLavoroviewPDF==1) ? "checked='checked'" : "";?> value="1"/>
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. previdenze private</label>
								<input type="number" name="prevPrivCount" id="prevPriv" class="form-control" value="<?php echo $this->client->prevPriv; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Vis. PDF se vuota</label>
								<input type="checkbox" class="form-control" id="prevPrivviewPDF" name="prevPrivviewPDF" <?php echo ($this->client->prevPrivviewPDF==1) ? "checked='checked'" : "";?> value="1"/>
							</div>
							<div class="clearfix"></div>
							<div class="panel panel-default" >
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#prevObbliContainer">Previdenza Obbligatoria</a></h2></div>
								<div class="panel-body collapse" id="prevObbliContainer">

									<?php foreach($this->client->prevObbliArray as $key=>$obbli) { ?>
										<div class="prevOblliContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="prevObbli[<?php echo $key; ?>][name]" id="prevObbli[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Data Prima Contribuzione</label>
													<div class='input-group datepicker' id="">
														<input type='text' class="form-control" id="prevObbli[<?php echo $key; ?>][prima]" name="prevObbli[<?php echo $key; ?>][prima]" value="<?php echo(strtotime($obbli->primaContr) ? date("d/m/Y H:i", strtotime($obbli->primaContr)) : date("d/m/Y")) ?>"/>
														<span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
													</div>
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Data Ultima Contribuzione</label>
													<div class='input-group datepicker' id="">
														<input type='text' class="form-control" id='prevObbli[<?php echo $key; ?>][ultima]' name="prevObbli[<?php echo $key; ?>][ultima]" value="<?php echo(strtotime($obbli->ultimaContr) ? date("d/m/Y H:i", strtotime($obbli->ultimaContr)) : date("d/m/Y")) ?>"/>
														<span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="calcAnzianita">
													<div class='field col-sm-4 col-md-4 col-xs-6'>
														<label>Anzianità Contributiva Settimane</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][anzianitaSett]" id="prevObbli[<?php echo $key; ?>][anzianitaSett]" class="form-control calcAnz" data-type="sett" value="<?php echo $obbli->anzianitaContrSett; ?>">
													</div>
													<div class='field col-sm-4 col-md-4 col-xs-6'>
														<label>Anzianità Contributiva Mesi</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][anzianitaMesi]" id="prevObbli[<?php echo $key; ?>][anzianitaMesi]" class="form-control calcAnz" data-type="mesi" value="<?php echo $obbli->anzianitaContrMesi; ?>">
													</div>
													<div class='field col-sm-4 col-md-4 col-xs-6'>
														<label>Anzianità Contributiva Anni</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][anzianitaAnni]" id="prevObbli[<?php echo $key; ?>][anzianitaAnni]" class="form-control calcAnz" data-type="ann" value="<?php echo $obbli->anzianitaContrAnni; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Montante Contributivo</label>
													<input type="text" name="prevObbli[<?php echo $key; ?>][monte]" id="prevObbli[<?php echo $key; ?>][monte]" class="form-control" value="<?php echo $obbli->monteContr?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>RAL</label>
													<input type="text" name="prevObbli[<?php echo $key; ?>][ral]" id="prevObbli[<?php echo $key; ?>][ral]" class="form-control" value="<?php echo $obbli->RAL; ?>">
												</div>
												<div class="clearfix"></div>
												<div class="calcann">
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita già acquisita mensile</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][renditaAcquisitaMens]" id="prevObbli[<?php echo $key; ?>][renditaAcquisitaMens]" class="mens form-control numeric" value="<?php echo $obbli->renditaAcquisitaMens; ?>">
													</div>
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita già acquisita x13</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][renditaAcquisitaAnn]" id="prevObbli[<?php echo $key; ?>][renditaAcquisitaAnn]" class="ann form-control numeric" value="<?php echo $obbli->renditaAcquisitaAnn; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="calcann">
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita Annua aggiuntiva mensile</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][renditaAnnuaAggMens]" id="prevObbli[<?php echo $key; ?>][renditaAnnuaAggMens]" class="mens form-control" value="<?php echo $obbli->renditaAnnuaAggMens; ?>">
													</div>
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita Annua aggiuntiva x13</label>
														<input type="text" name="prevObbli[<?php echo $key; ?>][renditaAnnuaAggAnn]" id="prevObbli[<?php echo $key; ?>][renditaAnnuaAggAnn]" class="ann form-control" value="<?php echo $obbli->renditaAnnuaAggAnn; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="panel panel-default erogscontainer">
													<div class="panel-heading"><h2>Erogazioni</h2><input type="text" name="" data-prevId="<?php echo $key; ?>" data-prevType="prevObbli" id="" class="addErogsCount form-control" value="<?php echo count($obbli->erogResults); ?>"></div>
													<div class="panel-body erogs" >
														<?php foreach($obbli->erogResults as $key2=>$erog) { ?>
															<div class="erogscontent">
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>Tipo Erogazione</label>
																	<select name="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][erogType]" id="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][erogType]" class="form-control actChosen">
																		<option value=""></option>
																		<?php foreach ($this->erogsTypes as $erogType) {
																			$selected = ($erogType->erogType==$erog->erogType) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $erogType->erogType; ?>" <?php echo $selected; ?>><?php echo $erogType->erogName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class="calceta">
																	<div class='field col-sm-4 col-md-2 col-xs-6'>
																		<label>Data</label>
																		<div class='input-group datepicker' id="">
																			<input type='text' class="data form-control" id='prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][data]' name="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][data]" value="<?php echo(strtotime($erog->data) ? date("d/m/Y", strtotime($erog->data)) : date("d/m/Y")) ?>"/>
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																	</div>
																	<div class='field col-sm-4 col-md-2 col-xs-6'>
																		<label>Età</label>
																		<input type="text" name="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][eta]" id="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][eta]" class="eta form-control" value="<?php echo $erog->eta; ?>">
																	</div>
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6 numeric'>
																	<label>Valore</label>
																	<input type="text" name="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][valore]" id="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][valore]" class="form-control" value="<?php echo $erog->valore; ?>">
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>in PDF</label>
																	<input type="checkbox" name="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][inPDF]" id="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][inPDF]" <?php echo ($erog->inPDF==1) ? "checked='checked'": ""; ?> class="form-control" value="1">
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>Vis. in PDF</label>
																	<select name="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][viewPDF]" id="prevObbli[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][viewPDF]" class="form-control">
																		<option value="1" <?php echo ($erog->viewPDF==1) ? "selected='selected'": ""; ?>>Mensile</option>
																		<option value="2" <?php echo ($erog->viewPDF==2) ? "selected='selected'": ""; ?>>Annuale</option>
																	</select>
																</div>
																<div class="clearfix"></div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="panel panel-default" id="">
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#prevLavoroContainer">Previdenza Aziendale</a></h2></div>
								<div class="panel-body collapse" id="prevLavoroContainer">
									<?php foreach($this->client->prevLavoroArray as $key=>$obbli) { ?>
										<div class="prevLavoroContent panel panel-default">
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="prevLavoro[<?php echo $key; ?>][name]" id="prevLavoro[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Data Prima Contribuzione</label>
													<div class='input-group datepicker' id="">
														<input type='text' class="form-control" id="prevLavoro[<?php echo $key; ?>][prima]" name="prevLavoro[<?php echo $key; ?>][prima]" value="<?php echo(strtotime($obbli->primaContr) ? date("d/m/Y H:i", strtotime($obbli->primaContr)) : date("d/m/Y")) ?>"/>
														<span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
													</div>
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Data Ultima Contribuzione</label>
													<div class='input-group datepicker' id="">
														<input type='text' class="form-control" id='prevLavoro[<?php echo $key; ?>][ultima]' name="prevLavoro[<?php echo $key; ?>][ultima]" value="<?php echo(strtotime($obbli->ultimaContr) ? date("d/m/Y H:i", strtotime($obbli->ultimaContr)) : date("d/m/Y")) ?>"/>
														<span class="input-group-addon">
                                                    <span class="glyphicon glyphicon-calendar"></span>
                                                </span>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Settimane/Mesi/Anni</label>
													<select name="prevLavoro[<?php echo $key; ?>][anzianitaContrType]" id="prevLavoro[<?php echo $key; ?>][anzianitaContrType]" class="form-control">
														<option value="1" <?php echo ($obbli->anzianitaContrType==1) ? "selected='selected'": ""; ?>>Settimane</option>
														<option value="2" <?php echo ($obbli->anzianitaContrType==2) ? "selected='selected'": ""; ?>>Mesi</option>
														<option value="3" <?php echo ($obbli->anzianitaContrType==3) ? "selected='selected'": ""; ?>>Anni</option>
													</select>
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Anzianità Contributiva</label>
													<input type="text" name="prevLavoro[<?php echo $key; ?>][anzianita]" id="prevLavoro[<?php echo $key; ?>][anzianita]" class="form-control" value="<?php echo $obbli->anzianitaContr?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Montante Contributivo</label>
													<input type="text" name="prevLavoro[<?php echo $key; ?>][monte]" id="prevLavoro[<?php echo $key; ?>][monte]" class="form-control" value="<?php echo $obbli->monteContr?>">
												</div>
												<!--<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>RAL</label>
													<input type="text" name="prevLavoro[<?php /*echo $key; */?>][ral]" id="prevLavoro[<?php /*echo $key; */?>][ral]" class="form-control" value="<?php /*echo $obbli->RAL*/?>">
												</div>-->
												<div class="clearfix"></div>
												<div class="calcann12">
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita già acquisita mensile</label>
														<input type="text" name="prevLavoro[<?php echo $key; ?>][renditaAcquisitaMens]" id="prevLavoro[<?php echo $key; ?>][renditaAcquisitaMens]" class="mens form-control" value="<?php echo $obbli->renditaAcquisitaMens; ?>">
													</div>
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita già acquisita x12</label>
														<input type="text" name="prevLavoro[<?php echo $key; ?>][renditaAcquisitaAnn]" id="prevLavoro[<?php echo $key; ?>][renditaAcquisitaAnn]" class="ann form-control" value="<?php echo $obbli->renditaAcquisitaAnn; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="calcann12">
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita Annua aggiuntiva mensile</label>
														<input type="text" name="prevLavoro[<?php echo $key; ?>][renditaAnnuaAggMens]" id="prevLavoro[<?php echo $key; ?>][renditaAnnuaAggMens]" class="mens form-control" value="<?php echo $obbli->renditaAnnuaAggMens; ?>">
													</div>
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita Annua aggiuntiva x12</label>
														<input type="text" name="prevLavoro[<?php echo $key; ?>][renditaAnnuaAggAnn]" id="prevLavoro[<?php echo $key; ?>][renditaAnnuaAggAnn]" class="ann form-control" value="<?php echo $obbli->renditaAnnuaAggAnn; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="panel panel-default erogscontainer">
													<div class="panel-heading"><h2>Erogazioni</h2><input type="text" name="" data-prevId="<?php echo $key; ?>" data-prevType="prevLavoro" id="" class="addErogsCount form-control" value="<?php echo count($obbli->erogResults); ?>"></div>
													<div class="panel-body erogs" >
														<?php foreach($obbli->erogResults as $key2=>$erog) { ?>
															<div class="erogscontent">
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>Tipo Erogazione</label>
																	<select name="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][erogType]" id="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][erogType]" class="form-control actChosen">
																		<option value=""></option>
																		<?php foreach ($this->erogsTypes as $erogType) {
																			$selected = ($erogType->erogType==$erog->erogType) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $erogType->erogType; ?>" <?php echo $selected; ?>><?php echo $erogType->erogName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class="calceta">
																	<div class='field col-sm-4 col-md-2 col-xs-6'>
																		<label>Data</label>
																		<div class='input-group datepicker' id="">
																			<input type='text' class="data form-control" id='prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][data]' name="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][data]" value="<?php echo(strtotime($erog->data) ? date("d/m/Y", strtotime($erog->data)) : date("d/m/Y")) ?>"/>
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																	</div>
																	<div class='field col-sm-4 col-md-2 col-xs-6'>
																		<label>Età</label>
																		<input type="text" name="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][eta]" id="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][eta]" class="eta form-control" value="<?php echo $erog->eta; ?>">
																	</div>
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6 numeric'>
																	<label>Valore</label>
																	<input type="text" name="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][valore]" id="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][valore]" class="form-control" value="<?php echo $erog->valore; ?>">
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>in PDF</label>
																	<input type="checkbox" name="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][inPDF]" id="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][inPDF]" <?php echo ($erog->inPDF==1) ? "checked='checked'": ""; ?> class="form-control" value="1">
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>Vis. in PDF</label>
																	<select name="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][viewPDF]" id="prevLavoro[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][viewPDF]" class="form-control">
																		<option value="1" <?php echo ($erog->viewPDF==1) ? "selected='selected'": ""; ?>>Mensile</option>
																		<option value="2" <?php echo ($erog->viewPDF==2) ? "selected='selected'": ""; ?>>Annuale</option>
																	</select>
																</div>
																<div class="clearfix"></div>
															</div>
														<?php } ?>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="panel panel-default renditeContainer">
													<div class="panel-heading"><h2>Rendite</h2><input type="text" name="" data-prevId="<?php echo $key; ?>" data-prevType="prevLavoro" id="" class="addRenditeCount form-control" value="<?php echo count($obbli->renditeResults); ?>"></div>
													<div class="panel-body rendite" >
														<?php foreach($obbli->renditeResults as $key2=>$erog) { ?>
															<div class="renditeContent">
																<div class='field col-sm-4 col-md-3 col-xs-6'>
																	<label>Tipo Rendita</label>
																	<select name="prevLavoro[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][erogType]" id="prevLavoro[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][erogType]" class="form-control actChosen tipoRendita">
																		<option value=""></option>
																		<?php foreach ($this->renditeTypes as $renditaType) {
																			$selected = ($renditaType->renditaType==$erog->renditaType) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $renditaType->renditaType; ?>" <?php echo $selected; ?>><?php echo $renditaType->renditaName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class='field col-sm-4 col-md-3 col-xs-6'>
																	<label>Modalità</label>
																	<select name="prevLavoro[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][modalita]" id="prevLavoro[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][modalita]" class="form-control actChosen modalita">
																		<option value=""></option>
																		<?php foreach ($this->modalita as $modalita) {
																			$selected = ($modalita->idModalita==$erog->modalita) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $modalita->idModalita; ?>" <?php echo $selected; ?>><?php echo $modalita->modalitaName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class='field col-sm-4 col-md-3 col-xs-6 numeric'>
																	<label>Valore</label>
																	<input type="text" name="prevLavoro[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][valore]" id="prevLavoro[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][valore]" class="form-control" value="<?php echo $erog->valore; ?>">
																</div>
																<div class="clearfix"></div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="panel panel-default" id="">
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#prevPrivContainer">Previdenza Privata</a></h2></div>
								<div class="panel-body collapse" id="prevPrivContainer">
									<?php foreach($this->client->prevPrivArray as $key=>$obbli) { ?>
										<div class="prevPrivContent panel panel-default">
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="prevPriv[<?php echo $key; ?>][name]" id="prevPriv[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Data Prima Contribuzione</label>
													<div class='input-group datepicker' id="">
														<input type='text' class="form-control" id="prevPriv[<?php echo $key; ?>][prima]" name="prevPriv[<?php echo $key; ?>][prima]" value="<?php echo(strtotime($obbli->primaContr) ? date("d/m/Y H:i", strtotime($obbli->primaContr)) : date("d/m/Y")) ?>"/>
														<span class="input-group-addon">
																<span class="glyphicon glyphicon-calendar"></span>
															</span>
													</div>
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>Data Ultima Contribuzione</label>
													<div class='input-group datepicker' id="">
														<input type='text' class="form-control" id='prevPriv[<?php echo $key; ?>][ultima]' name="prevPriv[<?php echo $key; ?>][ultima]" value="<?php echo(strtotime($obbli->ultimaContr) ? date("d/m/Y H:i", strtotime($obbli->ultimaContr)) : date("d/m/Y")) ?>"/>
														<span class="input-group-addon">
																<span class="glyphicon glyphicon-calendar"></span>
															</span>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Anzianità Contributiva</label>
													<input type="text" name="prevPriv[<?php echo $key; ?>][anzianita]" id="prevPriv[<?php echo $key; ?>][anzianita]" class="form-control" value="<?php echo $obbli->anzianitaContr?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Montante Contributivo</label>
													<input type="text" name="prevPriv[<?php echo $key; ?>][monte]" id="prevPriv[<?php echo $key; ?>][monte]" class="form-control" value="<?php echo $obbli->monteContr?>">
												</div>
												<!--<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>RAL</label>
													<input type="text" name="prevPriv[<?php /*echo $key; */?>][ral]" id="prevPriv[<?php /*echo $key; */?>][ral]" class="form-control" value="<?php /*echo $obbli->ral*/?>">
												</div>-->
												<div class="clearfix"></div>
												<div class="calcann12">
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita già acquisita mensile</label>
														<input type="text" name="prevPriv[<?php echo $key; ?>][renditaAcquisitaMens]" id="prevPriv[<?php echo $key; ?>][renditaAcquisitaMens]" class="mens form-control" value="<?php echo $obbli->renditaAcquisitaMens; ?>">
													</div>
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita già acquisita x12</label>
														<input type="text" name="prevPriv[<?php echo $key; ?>][renditaAcquisitaAnn]" id="prevPriv[<?php echo $key; ?>][renditaAcquisitaAnn]" class="ann form-control" value="<?php echo $obbli->renditaAcquisitaAnn; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="calcann12">
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita Annua aggiuntiva mensile</label>
														<input type="text" name="prevPriv[<?php echo $key; ?>][renditaAnnuaAggMens]" id="prevPriv[<?php echo $key; ?>][renditaAnnuaAggMens]" class="mens form-control" value="<?php echo $obbli->renditaAnnuaAggMens; ?>">
													</div>
													<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
														<label>Rendita Annua aggiuntiva x12</label>
														<input type="text" name="prevPriv[<?php echo $key; ?>][renditaAnnuaAggAnn]" id="prevPriv[<?php echo $key; ?>][renditaAnnuaAggAnn]" class="ann form-control" value="<?php echo $obbli->renditaAnnuaAggAnn; ?>">
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="panel panel-default erogscontainer">
													<div class="panel-heading"><h2>Erogazioni</h2><input type="text" name="" data-prevId="<?php echo $key; ?>" data-prevType="prevPriv" id="" class="addErogsCount form-control" value="<?php echo count($obbli->erogResults); ?>"></div>
													<div class="panel-body erogs" >
														<?php foreach($obbli->erogResults as $key2=>$erog) { ?>
															<div class="erogscontent">
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>Tipo Erogazione</label>
																	<select name="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][erogType]" id="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][erogType]" class="form-control actChosen">
																		<option value=""></option>
																		<?php foreach ($this->erogsTypes as $erogType) {
																			$selected = ($erogType->erogType==$erog->erogType) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $erogType->erogType; ?>" <?php echo $selected; ?>><?php echo $erogType->erogName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class="calceta">
																	<div class='field col-sm-3 col-md-2 col-xs-6'>
																		<label>Data</label>
																		<div class='input-group datepicker' id="">
																			<input type='text' class="data form-control" id='prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][data]' name="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][data]" value="<?php echo(strtotime($erog->data) ? date("d/m/Y", strtotime($erog->data)) : date("d/m/Y")) ?>"/>
																			<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
																		</div>
																	</div>
																	<div class='field col-sm-4 col-md-2 col-xs-6'>
																		<label>Età</label>
																		<input type="text" name="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][eta]" id="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][eta]" class="eta form-control" value="<?php echo $erog->eta; ?>">
																	</div>
																</div>

																<div class='field col-sm-4 col-md-2 col-xs-6 numeric'>
																	<label>Valore</label>
																	<input type="text" name="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][valore]" id="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][valore]" class="form-control" value="<?php echo $erog->valore; ?>">
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>in PDF</label>
																	<input type="checkbox" name="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][inPDF]" id="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][inPDF]" <?php echo ($erog->inPDF==1) ? "checked='checked'": ""; ?> class="form-control" value="1">
																</div>
																<div class='field col-sm-4 col-md-2 col-xs-6'>
																	<label>Vis. in PDF</label>
																	<select name="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][viewPDF]" id="prevPriv[<?php echo $key; ?>][erogs][<?php echo $key2; ?>][viewPDF]" class="form-control">
																		<option value="1" <?php echo ($erog->viewPDF==1) ? "selected='selected'": ""; ?>>Mensile</option>
																		<option value="2" <?php echo ($erog->viewPDF==2) ? "selected='selected'": ""; ?>>Annuale</option>
																	</select>
																</div>
																<div class="clearfix"></div>
															</div>
														<?php } ?>
													</div>
												</div>
												<div class="clearfix"></div>
												<div class="panel panel-default renditeContainer">
													<div class="panel-heading"><h2>Rendite</h2><input type="text" name="" data-prevId="<?php echo $key; ?>" data-prevType="prevPriv" id="" class="addRenditeCount form-control" value="<?php echo count($obbli->renditeResults); ?>"></div>
													<div class="panel-body rendite" >
														<?php foreach($obbli->renditeResults as $key2=>$erog) { ?>
															<div class="renditeContent">
																<div class='field col-sm-4 col-md-3 col-xs-6'>
																	<label>Tipo Rendita</label>
																	<select name="prevPriv[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][erogType]" id="prevPriv[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][erogType]" class="form-control actChosen tipoRendita">
																		<option value=""></option>
																		<?php foreach ($this->renditeTypes as $renditaType) {
																			$selected = ($renditaType->renditaType==$erog->renditaType) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $renditaType->renditaType; ?>" <?php echo $selected; ?>><?php echo $renditaType->renditaName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class='field col-sm-4 col-md-3 col-xs-6'>
																	<label>Modalità</label>
																	<select name="prevPriv[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][modalita]" id="prevPriv[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][modalita]" class="form-control actChosen modalita">
																		<option value=""></option>
																		<?php foreach ($this->modalita as $modalita) {
																			$selected = ($modalita->idModalita==$erog->modalita) ? "selected='selected'": ""; ?>
																			<option value="<?php echo $modalita->idModalita; ?>" <?php echo $selected; ?>><?php echo $modalita->modalitaName; ?></option>
																		<?php } ?>
																	</select>
																</div>
																<div class='field col-sm-4 col-md-3 col-xs-6 numeric'>
																	<label>Valore</label>
																	<input type="text" name="prevPriv[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][valore]" id="prevPriv[<?php echo $key; ?>][rendite][<?php echo $key2; ?>][valore]" class="form-control" value="<?php echo $erog->valore; ?>">
																</div>
																<div class="clearfix"></div>
															</div>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
							<div class="clearfix"></div>

							<div class='field col-sm-4 col-md-10 col-xs-6'>
								<label>Principali Evidenze</label>
								<?php
								$editor = JFactory::getEditor();
								echo $editor->display('analisi', $this->client->analisi, '550', '400', '60', '20', false);
								?>
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Visualizza in PDF</label>
								<input type="checkbox" name="diagnosiInPDF" id="diagnosiInPDF" <?php echo ($this->client->diagnosiInPDF) ? "checked='checked'" : "" ; ?> class="form-control" value="1">
							</div>

							<div class="clearfix"></div>
							<div class='field col-sm-4 col-md-10 col-xs-6'>
								<label>Prossimi Passi</label>
								<?php
								$editor = JFactory::getEditor();
								echo $editor->display('conclusioni', $this->client->conclusioni, '550', '400', '60', '20', false);
								?>
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Visualizza in PDF</label>
								<input type="checkbox" name="conclusioniInPDF" id="conclusioniInPDF" <?php echo ($this->client->conclusioniInPDF) ? "checked='checked'" : "" ; ?> class="form-control" value="1">
							</div>
							<div class="clearfix"></div>
							<div class='field col-sm-4 col-md-10 col-xs-6'>
								<label>Analisi Estratto Conto Contributivo</label>
								<?php
								$editor = JFactory::getEditor();
								echo $editor->display('AnalisiEstrattoConto', $this->client->AnalisiEstrattoConto, '550', '400', '60', '20', false);
								?>
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Visualizza in PDF</label>
								<input type="checkbox" name="analisiInPDF" id="analisiInPDF" <?php echo ($this->client->analisiInPDF) ? "checked='checked'" : "" ; ?> class="form-control" value="1">
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="profile">
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. protezioni obbligatorie</label>
								<input type="number" name="protObbliCount" id="protObbliCount" class="form-control" value="<?php echo $this->client->protObbli; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. protezioni lavoro</label>
								<input type="number" name="protLavCount" id="protLavCount" class="form-control" value="<?php echo $this->client->protLav; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. protezioni private</label>
								<input type="number" name="protPrivCount" id="protPrivCount" class="form-control" value="<?php echo $this->client->protPriv; ?>">
							</div>
							<div class="clearfix"></div>
							<div class="panel panel-default" >
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#protObbliContainer">Protezione Obbligatoria</a></h2></div>
								<div class="panel-body collapse" id="protObbliContainer">

									<?php foreach($this->client->protObbliArray as $key=>$obbli) { ?>
										<div class="protOblliContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="protObbli[<?php echo $key; ?>][name]" id="protObbli[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Requsito raggiunto</label>
													<input type="checkbox" name="protObbli[<?php echo $key; ?>][requisitoRaggiunto]" id="protObbli[<?php echo $key; ?>][requisitoRaggiunto]" class="form-control" value="<?php echo $obbli->requisitoRaggiunto; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Morte Valore Mensile</label>
													<input type="text" name="protObbli[<?php echo $key; ?>][morteMens]" id="protObbli[<?php echo $key; ?>][morteMens]" class="form-control" value="<?php echo $obbli->morteMens; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Morte Valore Annuale</label>
													<input type="text" name="protObbli[<?php echo $key; ?>][morteAnn]" id="protObbli[<?php echo $key; ?>][morteAnn]" class="form-control" value="<?php echo $obbli->morteAnn; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Inabilità Valore Mensile</label>
													<input type="text" name="protObbli[<?php echo $key; ?>][inabilitaMens]" id="protObbli[<?php echo $key; ?>][inabilitaMens]" class="form-control" value="<?php echo $obbli->inabilitaMens; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Inabilità Valore Annuale</label>
													<input type="text" name="protObbli[<?php echo $key; ?>][inabilitaAnn]" id="protObbli[<?php echo $key; ?>][inabilitaAnn]" class="form-control" value="<?php echo $obbli->inabilitaAnn; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Invalidità Valore Mensile</label>
													<input type="text" name="protObbli[<?php echo $key; ?>][invaliditaMens]" id="protObbli[<?php echo $key; ?>][invaliditaMens]" class="form-control" value="<?php echo $obbli->invaliditaMens; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Invalidità Valore Annuale</label>
													<input type="text" name="protObbli[<?php echo $key; ?>][invaliditaAnn]" id="protObbli[<?php echo $key; ?>][invaliditaAnn]" class="form-control" value="<?php echo $obbli->invaliditaAnn; ?>">
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#protLavContainer">Protezione Lavoro</a></h2></div>
								<div class="panel-body collapse" id="protLavContainer">

									<?php foreach($this->client->protLavArray as $key=>$obbli) { ?>
										<div class="protLavContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="protLav[<?php echo $key; ?>][name]" id="protLav[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-12 col-xs-6 numeric'>
													<label>Morte</label>
													<input type="text" name="protLav[<?php echo $key; ?>][morte]" id="protLav[<?php echo $key; ?>][morte]" class="form-control" value="<?php echo $obbli->morte; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Invalidità permanente infortunio</label>
													<input type="text" name="protLav[<?php echo $key; ?>][invaliditaInfortunio]" id="protLav[<?php echo $key; ?>][invaliditaInfortunio]" class="form-control" value="<?php echo $obbli->invaliditaInfortunio; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Invalidità permanente malattia</label>
													<input type="text" name="protLav[<?php echo $key; ?>][invaliditaMalattia]" id="protLav[<?php echo $key; ?>][invaliditaMalattia]" class="form-control" value="<?php echo $obbli->invaliditaMalattia; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>LTC</label>
													<input type="checkbox" name="protLav[<?php echo $key; ?>][LTC]" id="protLav[<?php echo $key; ?>][LTC]" class="form-control" value="<?php echo $obbli->LTC; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Valore</label>
													<input type="text" name="protLav[<?php echo $key; ?>][LTCVal]" id="protLav[<?php echo $key; ?>][LTCVal]" class="form-control" value="<?php echo $obbli->LTCVal; ?>">
												</div>
												<div class="clearfix"></div>

											</div>
										</div>
									<?php } ?>
								</div>
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#protPrivContainer">Protezione Privata</a></h2></div>
								<div class="panel-body collapse" id="protPrivContainer">

									<?php foreach($this->client->protPrivArray as $key=>$obbli) { ?>
										<div class="protPrivContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="protPriv[<?php echo $key; ?>][name]" id="protPriv[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-12 col-xs-6 numeric'>
													<label>Morte</label>
													<input type="text" name="protPriv[<?php echo $key; ?>][morte]" id="protPriv[<?php echo $key; ?>][morte]" class="form-control" value="<?php echo $obbli->morte; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Invalidità permanente infortunio</label>
													<input type="text" name="protPriv[<?php echo $key; ?>][invaliditaInfortunio]" id="protPriv[<?php echo $key; ?>][invaliditaInfortunio]" class="form-control" value="<?php echo $obbli->invaliditaInfortunio; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Invalidità permanente malattia</label>
													<input type="text" name="protPriv[<?php echo $key; ?>][invaliditaMalattia]" id="protPriv[<?php echo $key; ?>][invaliditaMalattia]" class="form-control" value="<?php echo $obbli->invaliditaMalattia; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6'>
													<label>LTC</label>
													<input type="checkbox" name="protPriv[<?php echo $key; ?>][LTC]" id="protPriv[<?php echo $key; ?>][LTC]" class="form-control" value="<?php echo $obbli->LTC; ?>">
												</div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Valore</label>
													<input type="text" name="protPriv[<?php echo $key; ?>][LTCVal]" id="protPriv[<?php echo $key; ?>][LTCVal]" class="form-control" value="<?php echo $obbli->LTCVal; ?>">
												</div>
												<div class="clearfix"></div>

											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="messages">
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. tutele sanitarie lavoro</label>
								<input type="number" name="tutLavCount" id="tutLavCount" class="form-control" value="<?php echo $this->client->tutLav; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. tutele sanitarie private</label>
								<input type="number" name="tutPrivCount" id="tutPrivCount" class="form-control" value="<?php echo $this->client->tutPriv; ?>">
							</div>
							<div class='field col-sm-4 col-md-2 col-xs-6'>
								<label>Nr. tutele sanitarie CCNL</label>
								<input type="number" name="tutCcnlCount" id="tutCcnlCount" class="form-control" value="<?php echo $this->client->tutCcnl; ?>">
							</div>
							<div class="clearfix"></div>
							<div class="panel panel-default" >
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#tutLavContainer">tutele sanitarie lavoro</a></h2></div>
								<div class="panel-body collapse" id="tutLavContainer">

									<?php foreach($this->client->tutLavArray as $key=>$obbli) { ?>
										<div class="tutLavContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="tutLav[<?php echo $key; ?>][name]" id="tutLav[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nomeTut) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Costo Annuo</label>
													<input type="text" name="tutLav[<?php echo $key; ?>][costoAnn]" id="tutLav[<?php echo $key; ?>][costoAnn]" class="form-control" value="<?php echo $obbli->costoAnn; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Prevenzione</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][prevenzione]" id="tutLav[<?php echo $key; ?>][prevenzione]" <?php echo ($obbli->prevenzione) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->prevenzione; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Diagnosi</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][diagnosi]" id="tutLav[<?php echo $key; ?>][diagnosi]" <?php echo ($obbli->diagnosi) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->diagnosi; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Interventi</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][interventi]" id="tutLav[<?php echo $key; ?>][interventi]" <?php echo ($obbli->interventi) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->interventi; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Riabilitazione</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][riabilitazione]" id="tutLav[<?php echo $key; ?>][riabilitazione]" <?php echo ($obbli->riabilitazione) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->riabilitazione; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Lenti e Denti</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][lentiDenti]" id="tutLav[<?php echo $key; ?>][lentiDenti]" <?php echo ($obbli->lentiDenti) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->lentiDenti; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>LTC</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][LTC]" id="tutLav[<?php echo $key; ?>][LTC]" <?php echo ($obbli->LTC) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->LTC; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Assistenza</label>
													<input type="checkbox" name="tutLav[<?php echo $key; ?>][assistenza]" id="tutLav[<?php echo $key; ?>][assistenza]" <?php echo ($obbli->assistenza) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->assistenza; ?>">
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#tutPrivContainer">tutele sanitarie private</a></h2></div>
								<div class="panel-body collapse" id="tutPrivContainer">

									<?php foreach($this->client->tutPrivArray as $key=>$obbli) { ?>
										<div class="tutPrivContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="tutPriv[<?php echo $key; ?>][name]" id="tutPriv[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nomeTut) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Costo Annuo</label>
													<input type="text" name="tutPriv[<?php echo $key; ?>][costoAnn]" id="tutPriv[<?php echo $key; ?>][costoAnn]" class="form-control" value="<?php echo $obbli->costoAnn; ?>">
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Prevenzione</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][prevenzione]" id="tutPriv[<?php echo $key; ?>][prevenzione]" <?php echo ($obbli->prevenzione) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->prevenzione; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Diagnosi</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][diagnosi]" id="tutPriv[<?php echo $key; ?>][diagnosi]" <?php echo ($obbli->diagnosi) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->diagnosi; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Interventi</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][interventi]" id="tutPriv[<?php echo $key; ?>][interventi]" <?php echo ($obbli->interventi) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->interventi; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Riabilitazione</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][riabilitazione]" id="tutPriv[<?php echo $key; ?>][riabilitazione]" <?php echo ($obbli->riabilitazione) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->riabilitazione; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Lenti e Denti</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][lentiDenti]" id="tutPriv[<?php echo $key; ?>][lentiDenti]" <?php echo ($obbli->lentiDenti) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->lentiDenti; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>LTC</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][LTC]" id="tutPriv[<?php echo $key; ?>][LTC]" <?php echo ($obbli->LTC) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->LTC; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Assistenza</label>
													<input type="checkbox" name="tutPriv[<?php echo $key; ?>][assistenza]" id="tutPriv[<?php echo $key; ?>][assistenza]" <?php echo ($obbli->assistenza) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->assistenza; ?>">
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="panel-heading"><h2><a data-toggle="collapse" href="#tutCcnlContainer">tutele sanitarie CCNL</a></h2></div>
								<div class="panel-body collapse" id="tutCcnlContainer">

									<?php foreach($this->client->tutCcnlArray as $key=>$obbli) { ?>
										<div class="tutCcnlContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
											<div class="panel-body">
												<div class='field col-sm-4 col-md-12 col-xs-6'>
													<label>Nome Cassa</label>
													<select name="tutCcnl[<?php echo $key; ?>][name]" id="tutCcnl[<?php echo $key; ?>][name]" class="form-control actChosen nomeCassa">
														<option value=""></option>
														<?php foreach ($this->casse as $cassa) {
															$selected = ($cassa->idCassa==$obbli->nomeTut) ? "selected='selected'": ""; ?>
															<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
														<?php } ?>
													</select>
												</div>
												<div class="clearfix"></div>
												<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
													<label>Costo Annuo</label>
													<input type="text" name="tutCcnl[<?php echo $key; ?>][costoAnn]" id="tutCcnl[<?php echo $key; ?>][costoAnn]" class="form-control" value="<?php echo $obbli->costoAnn; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Prevenzione</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][prevenzione]" id="tutCcnl[<?php echo $key; ?>][prevenzione]" <?php echo ($obbli->prevenzione) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->prevenzione; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Diagnosi</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][diagnosi]" id="tutCcnl[<?php echo $key; ?>][diagnosi]" <?php echo ($obbli->diagnosi) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->diagnosi; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Interventi</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][interventi]" id="tutCcnl[<?php echo $key; ?>][interventi]" <?php echo ($obbli->interventi) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->interventi; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Riabilitazione</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][riabilitazione]" id="tutCcnl[<?php echo $key; ?>][riabilitazione]" <?php echo ($obbli->riabilitazione) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->riabilitazione; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Lenti e Denti</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][lentiDenti]" id="tutCcnl[<?php echo $key; ?>][lentiDenti]" <?php echo ($obbli->lentiDenti) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->lentiDenti; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>LTC</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][LTC]" id="tutCcnl[<?php echo $key; ?>][LTC]" <?php echo ($obbli->LTC) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->LTC; ?>">
												</div>

												<div class='field col-sm-4 col-md-1 col-xs-6'>
													<label>Assistenza</label>
													<input type="checkbox" name="tutCcnl[<?php echo $key; ?>][assistenza]" id="tutCcnl[<?php echo $key; ?>][assistenza]" <?php echo ($obbli->assistenza) ? "checked='checked'" : ""; ?> class="form-control" value="<?php echo $obbli->assistenza; ?>">
												</div>
												<div class="clearfix"></div>
											</div>
										</div>
									<?php } ?>
								</div>
							</div>
						</div>
					</div>

                </fieldset>
                <?php echo JHtml::_('form.token'); ?>
            </form>
        </div>
    </div>
</div>

<div id="addErogs" style="display: none;">
    <div class="erogscontent">
        <div class='field col-sm-4 col-md-2 col-xs-6'>
            <label>Tipo Erogazione</label>
			<select name="prevObbli[][erogs]{}[erogType]" id="prevObbli[][erogs]{}[erogType]" class="form-control actChosen tipoErog">
				<option value=""></option>
				<?php foreach ($this->erogsTypes as $erogType) { ?>
					<option value="<?php echo $erogType->erogType; ?>" <?php echo $selected; ?>><?php echo $erogType->erogName; ?></option>
				<?php } ?>
			</select>
        </div>
        <div class="calceta">
            <div class='field col-sm-4 col-md-2 col-xs-6'>
                <label>Data</label>
                <div class='input-group datepicker' id="">
                    <input type='text' class="data form-control" id='prevObbli[][erogs]{}[data]' name="prevObbli[][erogs]{}[data]" value="<?php echo date("d/m/Y"); ?>"/>
                    <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                </div>
            </div>
            <div class='field col-sm-4 col-md-2 col-xs-6'>
                <label>Età</label>
                <input type="text" name="prevObbli[][erogs]{}[eta]" id="prevObbli[][erogs]{}[eta]" class="eta form-control" value="">
            </div>
        </div>
        <div class='field col-sm-4 col-md-2 col-xs-6 numeric'>
            <label>Valore</label>
            <input type="text" name="prevObbli[][erogs]{}[valore]" id="prevObbli[][erogs]{}[valore]" class="form-control" value="">
        </div>
		<div class='field col-sm-4 col-md-2 col-xs-6'>
			<label>in PDF</label>
			<input type="checkbox" name="prevObbli[][erogs]{}[inPDF]" id="prevObbli[][erogs]{}[inPDF]" class="form-control" value="1">
		</div>
		<div class='field col-sm-4 col-md-2 col-xs-6'>
			<label>Vis. in PDF</label>
			<select name="prevObbli[][erogs]{}[viewPDF]" id="prevObbli[][erogs]{}[viewPDF]" class="form-control">
				<option value="1" >Mensile</option>
				<option value="2" >Annuale</option>
			</select>
		</div>
        <div class="clearfix"></div>
    </div>
</div>


<div id="addRendita" style="display: none;">
	<div class="renditeContent">
		<div class='field col-sm-4 col-md-3 col-xs-6'>
			<label>Tipo Rendita</label>
			<select name="prevObbli[][rendite]{}[renditaType]" id="prevObbli[][rendite]{}[renditaType]" class="form-control actChosen tipoRendita">
				<option value=""></option>
				<?php foreach ($this->renditeTypes as $renditaType) { ?>
					<option value="<?php echo $renditaType->renditaType; ?>"><?php echo $renditaType->renditaName; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class='field col-sm-4 col-md-3 col-xs-6'>
			<label>Modalità</label>
			<select name="prevObbli[][rendite]{}[modalita]" id="prevObbli[][rendite]{}[modalita]" class="form-control actChosen modalita">
				<option value=""></option>
				<?php foreach ($this->modalita as $modalita) { ?>
					<option value="<?php echo $modalita->idModalita; ?>"><?php echo $modalita->modalitaName; ?></option>
				<?php } ?>
			</select>
		</div>
		<div class='field col-sm-4 col-md-3 col-xs-6 numeric'>
			<label>Valore</label>
			<input type="text" name="prevObbli[][rendite]{}[valore]" id="prevObbli[][rendite]{}[valore]" class="form-control" value="">
		</div>
		<div class="clearfix"></div>
	</div>
</div>

<div id="addProtObbli" style="display: none;">
	<div class="protOblliContent panel panel-default" id="" >
		<div class="panel-body">
			<div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Nome Cassa</label>
				<select name="protObbli[][name]" id="protObbli[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
					<?php foreach ($this->casse as $cassa) {
						$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
						<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Requsito raggiunto</label>
				<input type="checkbox" name="protObbli[][requisitoRaggiunto]" id="protObbli[][requisitoRaggiunto]" class="form-control" value="<?php echo $obbli->requisitoRaggiunto; ?>">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Morte Valore Mensile</label>
				<input type="text" name="protObbli[][morteMens]" id="protObbli[][morteMens]" class="form-control" value="">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Morte Valore Annuale</label>
				<input type="text" name="protObbli[][morteAnn]" id="protObbli[][morteAnn]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Inabilità Valore Mensile</label>
				<input type="text" name="protObbli[][inabilitaMens]" id="protObbli[][inabilitaMens]" class="form-control" value="">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Inabilità Valore Annuale</label>
				<input type="text" name="protObbli[][inabilitaAnn]" id="protObbli[][inabilitaAnn]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Invalidità Valore Mensile</label>
				<input type="text" name="protObbli[][invaliditaMens]" id="protObbli[][invaliditaMens]" class="form-control" value="">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Invalidità Valore Annuale</label>
				<input type="text" name="protObbli[][invaliditaAnn]" id="protObbli[][invaliditaAnn]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div id="addProtLav" style="display: none;">
	<div class="protOblliContent panel panel-default" id="" >
		<div class="panel-body">
			<div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Nome Cassa</label>
				<select name="protLav[][name]" id="protLav[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
					<?php foreach ($this->casse as $cassa) {
						$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
						<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-12 col-xs-6 numeric'>
				<label>Morte</label>
				<input type="text" name="protLav[][morte]" id="protLav[][morte]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Invalidità permanente infortunio</label>
				<input type="text" name="protLav[][invaliditaInfortunio]" id="protLav[][invaliditaInfortunio]" class="form-control" value="">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Invalidità permanente malattia</label>
				<input type="text" name="protLav[][invaliditaMalattia]" id="protLav[][invaliditaMalattia]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6'>
				<label>LTC</label>
				<input type="checkbox" name="protLav[][LTC]" id="protLav[][LTC]" class="form-control" value="1">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Valore</label>
				<input type="text" name="protLav[][LTCVal]" id="protLav[][LTCVal]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>

<div id="addProtPriv" style="display: none;">
	<div class="protOblliContent panel panel-default" id="" >
		<div class="panel-body">
			<div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Nome Cassa</label>
				<select name="protPriv[][name]" id="protPriv[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
					<?php foreach ($this->casse as $cassa) {
						$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
						<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-12 col-xs-6 numeric'>
				<label>Morte</label>
				<input type="text" name="protPriv[][morte]" id="protPriv[][morte]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Invalidità permanente infortunio</label>
				<input type="text" name="protPriv[][invaliditaInfortunio]" id="protPriv[][invaliditaInfortunio]" class="form-control" value="">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Invalidità permanente malattia</label>
				<input type="text" name="protPriv[][invaliditaMalattia]" id="protPriv[][invaliditaMalattia]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6'>
				<label>LTC</label>
				<input type="checkbox" name="protPriv[][LTC]" id="protPriv[][LTC]" class="form-control" value="1">
			</div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Valore</label>
				<input type="text" name="protPriv[][LTCVal]" id="protPriv[][LTCVal]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>

		</div>
	</div>
</div>

<div id="addTut" style="display: none;">
	<div class="tutLavContent panel panel-default" id="<?php echo $obbli->idObbli?>" >
		<div class="panel-body">
			<div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Nome Cassa</label>
				<select name="tutLav[][name]" id="tutLav[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
					<?php foreach ($this->casse as $cassa) {
						$selected = ($cassa->idCassa==$obbli->nome) ? "selected='selected'": ""; ?>
						<option value="<?php echo $cassa->idCassa; ?>" <?php echo $selected; ?>><?php echo $cassa->nomeCassa; ?></option>
					<?php } ?>
				</select>
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>Costo Annuo</label>
				<input type="text" name="tutLav[][costoAnn]" id="tutLav[][costoAnn]" class="form-control" value="">
			</div>
			<div class="clearfix"></div>
			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>Prevenzione</label>
				<input type="checkbox" name="tutLav[][prevenzione]" id="tutLav[][prevenzione]" class="form-control" value="1">
			</div>

			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>Diagnosi</label>
				<input type="checkbox" name="tutLav[][diagnosi]" id="tutLav[][diagnosi]" class="form-control" value="1">
			</div>

			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>Interventi</label>
				<input type="checkbox" name="tutLav[][interventi]" id="tutLav[][interventi]" class="form-control" value="1">
			</div>

			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>Riabilitazione</label>
				<input type="checkbox" name="tutLav[][riabilitazione]" id="tutLav[][riabilitazione]" class="form-control" value="1">
			</div>

			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>Lenti e Denti</label>
				<input type="checkbox" name="tutLav[][lentiDenti]" id="tutLav[][lentiDenti]" class="form-control" value="1">
			</div>

			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>LTC</label>
				<input type="checkbox" name="tutLav[][LTC]" id="tutLav[][LTC]" class="form-control" value="1">
			</div>

			<div class='field col-sm-4 col-md-1 col-xs-6'>
				<label>Assistenza</label>
				<input type="checkbox" name="tutLav[][assistenza]" id="tutLav[][assistenza]" class="form-control" value="1">
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

<div id="addPrevObbli" style="display: none;">
    <div class="prevOblliContent panel panel-default">
        <div class="panel-body">
            <div class='field col-sm-4 col-md-12 col-xs-6'>
                <label>Nome Cassa</label>
				<select name="prevObbli[][name]" id="prevObbli[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
		            <?php foreach ($this->casse as $cassa) { ?>
						<option value="<?php echo $cassa->idCassa; ?>"><?php echo $cassa->nomeCassa; ?></option>
		            <?php } ?>
				</select>
            </div>
            <div class="clearfix"></div>
            <div class='field col-sm-4 col-md-6 col-xs-6'>
                <label>Data Prima Contribuzione</label>
                <div class='input-group datepicker' id="">
                    <input type='text' class="form-control" id="prevObbli[][prima]" name="prevObbli[][prima]" value="<?php echo date("d/m/Y"); ?>"/>
                    <span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
                </div>
            </div>
            <div class='field col-sm-4 col-md-6 col-xs-6'>
                <label>Data Ultima Contribuzione</label>
                <div class='input-group datepicker' id="">
                    <input type='text' class="form-control" id='prevObbli[][ultima]' name="prevObbli[][ultima]" value="<?php echo date("d/m/Y"); ?>"/>
                    <span class="input-group-addon">
						<span class="glyphicon glyphicon-calendar"></span>
					</span>
                </div>
            </div>
            <div class="clearfix"></div>
			<div class="calcAnzianita">
				<div class='field col-sm-4 col-md-4 col-xs-6'>
					<label>Anzianità Contributiva Settimane</label>
					<input type="text" name="prevObbli[][anzianitaSett]" id="prevObbli[][anzianitaSett]" class="form-control calcAnz" data-type="sett" value="">
				</div>
				<div class='field col-sm-4 col-md-4 col-xs-6'>
					<label>Anzianità Contributiva Mesi</label>
					<input type="text" name="prevObbli[][anzianitaMesi]" id="prevObbli[][anzianitaMesi]" class="form-control calcAnz" data-type="mesi" value="">
				</div>
				<div class='field col-sm-4 col-md-4 col-xs-6'>
					<label>Anzianità Contributiva Anni</label>
					<input type="text" name="prevObbli[][anzianitaAnni]" id="prevObbli[][anzianitaAnni]" class="form-control calcAnz" data-type="ann" value="">
				</div>
			</div>
            <div class="clearfix"></div>
            <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
                <label>Montante Contributivo</label>
                <input type="text" name="prevObbli[][monte]" id="prevObbli[][monte]" class="form-control" value="">
            </div>
			<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>RAL</label>
				<input type="text" name="prevObbli[][ral]" id="prevObbli[][ral]" class="form-control" value="">
			</div>
            <div class="clearfix"></div>
            <div class="calcann">
                <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
                    <label>Rendita già acquisita mensile</label>
                    <input type="text" name="prevObbli[][renditaAcquisitaMens]" id="prevObbli[][renditaAcquisitaMens]" class="mens form-control" value="">
                </div>
                <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
                    <label>Rendita già acquisita x13</label>
                    <input type="text" name="prevObbli[][renditaAcquisitaAnn]" id="prevObbli[][renditaAcquisitaAnn]" class="ann form-control" value="">
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="calcann">
                <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
                    <label>Rendita Annua aggiuntiva mensile</label>
                    <input type="text" name="prevObbli[][renditaAnnuaAggMens]" id="prevObbli[][renditaAnnuaAggMens]" class="mens form-control" value="">
                </div>
                <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
                    <label>Rendita Annua aggiuntiva x13</label>
                    <input type="text" name="prevObbli[][renditaAnnuaAggAnn]" id="prevObbli[][renditaAnnuaAggAnn]" class="ann form-control" value="">
                </div>
            </div>
	        <div class="clearfix"></div>
	        <div class="panel panel-default erogscontainer">
		        <div class="panel-heading"><h2>Erogazioni</h2><input type="text" name="" data-prevId="--" data-prevType="prevObbli" id="" class="addErogsCount form-control" value=""></div>
		        <div class="panel-body erogs" >

		        </div>
	        </div>

        </div>
    </div>
</div>
<div id="addPrevLavoro" style="display: none;">
    <div class="prevLavoroContent">
	    <div class="panel-body">
		    <div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Nome Cassa</label>
				<select name="prevLavoro[][name]" id="prevLavoro[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
				    <?php foreach ($this->casse as $cassa) { ?>
						<option value="<?php echo $cassa->idCassa; ?>"><?php echo $cassa->nomeCassa; ?></option>
				    <?php } ?>
				</select>
		    </div>
		    <div class="clearfix"></div>
		    <div class='field col-sm-4 col-md-6 col-xs-6'>
			    <label>Data</label>
			    <div class='input-group datepicker' id="">
				    <input type='text' class="form-control" id="prevLavoro[][prima]" name="prevLavoro[][prima]" value="<?php echo date("d/m/Y"); ?>"/>
				    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
			    </div>
		    </div>
		    <div class='field col-sm-4 col-md-6 col-xs-6'>
			    <label>Data</label>
			    <div class='input-group datepicker' id="">
				    <input type='text' class="form-control" id='prevLavoro[][ultima]' name="prevLavoro[][ultima]" value="<?php echo date("d/m/Y"); ?>"/>
				    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
			    </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class='field col-sm-4 col-md-6 col-xs-6'>
			    <label>Anzianità Contributiva</label>
			    <input type="text" name="prevLavoro[][anzianita]" id="prevLavoro[][anzianita]" class="form-control" value="">
		    </div>
		    <div class="clearfix"></div>
		    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
			    <label>Montante Contributivo</label>
			    <input type="text" name="prevLavoro[][monte]" id="prevLavoro[][monte]" class="form-control" value="">
		    </div>
			<!--<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>RAL</label>
				<input type="text" name="prevLavoro[][ral]" id="prevLavoro[][ral]" class="form-control" value="">
			</div>-->
		    <div class="clearfix"></div>
		    <div class="calcann12">
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita già acquisita mensile</label>
				    <input type="text" name="prevLavoro[][renditaAcquisitaMens]" id="prevLavoro[][renditaAcquisitaMens]" class="mens form-control" value="">
			    </div>
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita già acquisita x12</label>
				    <input type="text" name="prevLavoro[][renditaAcquisitaAnn]" id="prevLavoro[][renditaAcquisitaAnn]" class="ann form-control" value="">
			    </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="calcann12">
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita Annua aggiuntiva mensile</label>
				    <input type="text" name="prevLavoro[][renditaAnnuaAggMens]" id="prevLavoro[][renditaAnnuaAggMens]" class="mens form-control" value="">
			    </div>
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita Annua aggiuntiva x12</label>
				    <input type="text" name="prevLavoro[][renditaAnnuaAggAnn]" id="prevLavoro[][renditaAnnuaAggAnn]" class="ann form-control" value="">
			    </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="panel panel-default erogscontainer">
			    <div class="panel-heading"><h2>Erogazioni</h2><input type="text" name="" data-prevId="--" data-prevType="prevLavoro" id="" class="addErogsCount form-control" value=""></div>
			    <div class="panel-body erogs" >
			    </div>
		    </div>
			<div class="clearfix"></div>
			<div class="panel panel-default renditeContainer">
				<div class="panel-heading"><h2>Rendite</h2><input type="text" name="" data-prevId="--" data-prevType="prevLavoro" id="" class="addRenditeCount form-control" value="<?php echo count($obbli->renditeResults); ?>"></div>
				<div class="panel-body rendite" >
				</div>
			</div>
	    </div>
    </div>
</div>
<div id="addPrevPriv" style="display: none;">
    <div class="prevPrivContent">
	    <div class="panel-body">
		    <div class='field col-sm-4 col-md-12 col-xs-6'>
				<label>Nome Cassa</label>
				<select name="prevPriv[][name]" id="prevPriv[][name]" class="form-control actChosen nomeCassa">
					<option value=""></option>
				    <?php foreach ($this->casse as $cassa) { ?>
						<option value="<?php echo $cassa->idCassa; ?>"><?php echo $cassa->nomeCassa; ?></option>
				    <?php } ?>
				</select>

			</div>
		    <div class="clearfix"></div>
		    <div class='field col-sm-4 col-md-6 col-xs-6'>
			    <label>Data</label>
			    <div class='input-group datepicker' id="">
				    <input type='text' class="form-control" id="prevPriv[][prima]" name="prevPriv[][prima]" value="<?php echo date("d/m/Y"); ?>"/>
				    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
			    </div>
		    </div>
		    <div class='field col-sm-4 col-md-6 col-xs-6'>
			    <label>Data</label>
			    <div class='input-group datepicker' id="">
				    <input type='text' class="form-control" id='prevPriv[][ultima]' name="prevPriv[][ultima]" value="<?php echo date("d/m/Y"); ?>"/>
				    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
			    </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class='field col-sm-4 col-md-6 col-xs-6'>
			    <label>Anzianità Contributiva</label>
			    <input type="text" name="prevPriv[][anzianita]" id="prevPriv[][anzianita]" class="form-control" value="">
		    </div>
		    <div class="clearfix"></div>
		    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
			    <label>Montante Contributivo</label>
			    <input type="text" name="prevPriv[][monte]" id="prevPriv[][monte]" class="form-control" value="">
		    </div>
			<!--<div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				<label>RAL</label>
				<input type="text" name="prevPriv[][ral]" id="prevPriv[][ral]" class="form-control" value="">
			</div>-->
		    <div class="clearfix"></div>
		    <div class="calcann12">
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita già acquisita mensile</label>
				    <input type="text" name="prevPriv[][renditaAcquisitaMens]" id="prevPriv[][renditaAcquisitaMens]" class="mens form-control" value="">
			    </div>
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita già acquisita x12</label>
				    <input type="text" name="prevPriv[][renditaAcquisitaAnn]" id="prevPriv[][renditaAcquisitaAnn]" class="ann form-control" value="">
			    </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="calcann12">
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita Annua aggiuntiva mensile</label>
				    <input type="text" name="prevPriv[][renditaAnnuaAggMens]" id="prevPriv[][renditaAnnuaAggMens]" class="mens form-control" value="">
			    </div>
			    <div class='field col-sm-4 col-md-6 col-xs-6 numeric'>
				    <label>Rendita Annua aggiuntiva x12</label>
				    <input type="text" name="prevPriv[][renditaAnnuaAggAnn]" id="prevPriv[][renditaAnnuaAggAnn]" class="ann form-control" value="">
			    </div>
		    </div>
		    <div class="clearfix"></div>
		    <div class="panel panel-default erogscontainer">
			    <div class="panel-heading"><h2>Erogazioni</h2><input type="text" name="" data-prevId="--" data-prevType="prevPriv" id="" class="addErogsCount form-control" value=""></div>
			    <div class="panel-body erogs" >
			    </div>
		    </div>
			<div class="clearfix"></div>
			<div class="panel panel-default renditeContainer">
				<div class="panel-heading"><h2>Rendite</h2><input type="text" name="" data-prevId="--" data-prevType="prevPriv" id="" class="addRenditeCount form-control" value="<?php echo count($obbli->renditeResults); ?>"></div>
				<div class="panel-body rendite" >
				</div>
			</div>
	    </div>
    </div>
</div>

<style>
	.chosen-container .chosen-drop {
		width: 250% !important;
	}
</style>